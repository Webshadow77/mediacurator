
# MediaCurator

Ein kleines Program, dass für das Verwalten von unterschiedlichen Medien/Dateien gedacht ist. Es ist am stärksten auf Bilder ausgelegt.
Das ganze funktioniert lokal am besten aber nicht ausschließlich in Docker und erreichbar über einen Web-Browser.
Man kann an Dateien so genante Tags anhäften und mit der Zeit nach diesen 
filtern. 

## Technische Vorraussätungen und Abhängigkeiten

Minimal wird Java17, Maven und eine Postgres Datenbank gebraucht. <br>
Die notwendigen Datenbank Attribute sind in der **application.properties** zu finden. <br>
Um das Projekt korrekt zu bauen ist die Datenban notwendig. In dem initial-sql Ordner sind alle Scripte abgelegt um alle Tabellen ect. in der Datenbank anzulegen. <br>
Wird mit **mvn package** das Projekt gebaut wird das JOOQ Plugin aktiv und verbindet sich zur DB um aus den Tabellen Java Classen zu generiren. <br>
Die generierten Klassen werden in src/generated/java abgelegt und wird prinzipiell automatisch zum Buildpath hinzugefügt. <br>
Nun kann das Projekt in der IDE gestartet werden. <br>

## Lokaler start

Wurde das Projekt mit **mvn package** gebaut, dann kann es lokal mittels Java oder in Docker gestartet werden. Hier wird beides gezeigt. <br>  <br> 

Mittels java kann man das Jar File das im target Ordner liegt an einen Ort der eigenen Wahl verschieben. <br> 
Danach kann mit einfach mit **java -jar mediacurator.jar** das Program starten. Es nimt zur Konfiguration die **application.properties** Datei. <br> 
Soll von anderen properties gestartet werden kann das mittels Spring Profile beim Start mitgegeben werden.
 <br>  <br> 
Mittels docker muss nach dem Build schritt mit Maven noch im Root Verzeichnes der Docker befehl zum erzeugen des Images ausgeführt werden.
Dazu sind auch notizen im **Dockerfile** hitnerlegt. Der Befehl ist **docker build -t deinDockerNutzer/deinDockerRepo:deinTag .**
Wird das Docker Image gestartet muss man ihm noch sagen welchen Port er freigeben soll und in welchem System Direktory er arbeiten kann. <br> 
`docker run -p 0.0.0.0:8080:8080/tcp --mount type=bind,src="C:\PfadZuDeinemVerzeichnis\tag-db-storage",dst=/tag-db-storage deinDockerNutzer/deinDockerRepo:deinTag` <br> 
Details zu dem  Aufruf im DockerFile.
Das **/tag-db-storage** Verzeichnis ist jenes in dem die Dateien durch den MediaCurator verwaltet werden. Er erzeugt automatisch Unterverzeichnisse.
 <br>  <br> 
Im Verzeichnes **/tag-db-storage/incomming** kann man neue Dateien einfügen, die automatisch erkannt und verwaltet werden.

## Benutzung

Ist das Projekt gestartet läuft es default auf **localhost:8080**  <br> 
Hier kann man beim ersten Start einen Nutzer mit Name anlegen. <br> 
Ist man angemeldet, kann man im oberen Seitenbereich im Bereich **Daten-Eingang** eine Auswahl aus dem **/tag-db-storage/incomming** Verzeichnis sehen. <br>
Hier kann man klicken und dann hoffentlich intuitiv die Dateien mit Tags versehen. Hat man das getan, wir die Datei in einen anderen Ordner verschoben und ist in der Anwendung im Bereich **Erkunden** zu finden.
Im Bereich **Erkunden** kann man getagte Dateien finden. Zunächst kriegt man eine halb zufällige Auswahl und wenn man nach Tags such entsprechen suchen und finden.<br>
Hat man was gefunden, kann man sie auswählen, betrachten, bearbeiten und hart die Datei vom PC löschen. Man kann auch die Tags kopieren und beim Taggen einer anderen Datei wiederverwenden um sich die Arbeit zu erleichtern. <br>
Im Tags Bereich kann man die Tags verwalten. Hier kann man auch ein Tag auf eine Blacklist setzen. Stellt man dann über **Profil** sein Profil noch auf **Saveserch** bekommt man bei Suchen nur die nicht geblacklisteten Dateien angezeigt.

## Technischer Aufbau und Details

### Frontend

Im Frontend wird immer mehr versucht rein nativ zu arbeiten. Zum transpilen wird das **NP Gulp Plugin** verwendet.
In **resources/static/** finden wird TS (Typescript) und SASS (SCSS) Ordner. Diese werden on the fly nach JS und CSS in die entsprechenden Ordner übersetzt/transpiliert
Aktuell verwendet das Projekt im CSS Bereich noch Bootsrapt, dass soll sich allerdings auch noch ändern.
Ein Teil des Frontends wird im Server gerandert, via Thymeleaf. Alle diese HTML Dateien findet man unter **resources/templates/**

### Backend

Im Backend wird alles in Java mit Spring gemacht und der Datenbankzugrif passiert mittels JOOQ. JOOQ sorgt für Typen sicheres und wiederverwendbares SQL. Die JOOQ Klassen werden direkt von der Datenbank generiert, so das sie immer den aktuellen Zustand der Datenbank wiederspiegeln.
<br>
<br>
Der Workflow ist für User und Applikation gleich.
1. Kommen Dateien im **/tag-db-storage/incomming** Ordner hinein, so erkennt das der Code der im **filestorage** Packet liegt. Hier finden alle IO Operationen statt.
2. Erkannte Dateien werden als TmpFile in der DB verzeichnet und können vom User getaggt werden. Das passiert alles im **tmp** Packet
3. Ist die Datei fertig getagt, wandert die Zuständigkeit final in das **file** Packet. Die Datei hat sich auf der Festplatte dann auch von /incomming zu einem Datei Typ entsprechendem Verzeichnes bewegt.

### Datenbank

Wie oben schon einmal erwähnt sind im **resources/initial-sql/** Ordner alle notwendigen SQL Dateien zum erzeugen der Datenbank Tabellen hinterlegt. Es wir hier nach einem festen Muster gearbeitet und einige Sachen manuell gemacht. Das sorgt dafür, dass einige Aufgaben, wie das setzten der Created-At Spalte, automatisch von der Datenbank gemacht wird.