FROM openjdk:17
MAINTAINER sebastian-mett.de
VOLUME /tmp /tag-db-storage
COPY target/mediacurator.jar mediacurator.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar", "-Dspring.profiles.active=prod", "/mediacurator.jar"]

# To build you can use: docker build -t webshadow77/sebastian-private-repo:mediacurator .
# this naming is <user-name>/<docker-hub-repo>:<tag>
# Run with docker run -p8080:8080 webshadow77/sebastian-private-repo:mediacurator
# zum start das lokale tag-db-storage verzeichnis in das Docker "/tag-db-storage" Verzeichnis linken

# docker run -p 0.0.0.0:8080:8080/tcp --mount type=bind,src="E:\Desktop\Bilderverwaltung\MediaCurator\tag-db-storage",dst=/tag-db-storage webshadow77/sebastian-private-repo:mediacurator


# -p 0.0.0.0:8080:8080/tcp 
# 0.0.0.0:8080 mein PC System
# 8080/tcp Docker System

# --mount type=bind,src="E:\Desktop\Bilderverwaltung\MediaCurator\tag-db-storage",dst=/tag-db-storage 
# src ist gebundener PC Ordner
# dst ist der im Docker zu verlinkende Ordner

# Cleanup -> docker rm -f $(docker ps -a -q)
# Entfernt alle idel befindlichen Container