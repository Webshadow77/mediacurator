'use strict';

var gulp = require('gulp');
var debug = require('gulp-debug');
var sass = require('gulp-sass');
var ts = require('gulp-typescript');

sass.compiler = require('node-sass');

gulp.task('sass', gulp.series(function() {
	return gulp.src('src/main/resources/static/sass/**/*.scss')
			.pipe(debug())
			.pipe(sass().on('error', sass.logError)).pipe(
					gulp.dest('src/main/resources/static/css'));
}));

gulp.task('typescript', gulp.series(function() {
    return gulp.src('src/main/resources/static/ts/**/*.ts')
			    .pipe(ts({
			        noImplicitAny: true,
			    	target: 'ES6',
			    	removeComments: true,
                    lib: ["ES6", "DOM"]
			    }))
			    .pipe(gulp.dest('src/main/resources/static/js'));
}));

// Default task. Run tasks sass and typescript in parallel
gulp.task('default', gulp.parallel('sass', 'typescript'));