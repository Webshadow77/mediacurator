package de.mett.mediacurator.securety.user;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import de.mett.db.public_.tables.records.AppUserRecord;

@Service
public class AppUserService {

	@Autowired
	private AppUserRepository userRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;

	public List<AppUserDto> findAll() {
		return userRepository.findAll();
	}

	public AppUserDto getUser(String name) {
		AppUserDto userList = userRepository.findDtoByUsername(name);
		if (userList != null) {
			return userList;
		}
		return null;
	}

	@Transactional
	public void registerUser(AppUserDto newUser) {
		newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
		AppUserRecord toDb = userRepository.newRecord();
		toDb.setUsername(newUser.getUsername());
    toDb.setPassword(newUser.getPassword());		
		userRepository.save(toDb);
	}

	public boolean swapSaveSearch(String userName) {
	  AppUserRecord user = userRepository.findByUsername(userName);
		if (user == null) {
			return false;
		}
		user.setSaveSearchEnabled(!user.getSaveSearchEnabled());
		userRepository.save(user);
		return true;
	}
}
