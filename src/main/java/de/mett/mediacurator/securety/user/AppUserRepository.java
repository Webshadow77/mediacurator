package de.mett.mediacurator.securety.user;

import java.util.List;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;

import de.mett.db.public_.tables.AppUser;
import de.mett.db.public_.tables.records.AppUserRecord;

@Repository
public class AppUserRepository {

  private static final AppUser tAppUser = AppUser.APP_USER;

  private final DSLContext jooq;

  public AppUserRepository(DSLContext jooq) {
    this.jooq = jooq;
  }

  public AppUserRecord newRecord() {
    return jooq.newRecord(tAppUser);
  }

  public void save(AppUserRecord newTmpFile) {
    newTmpFile.store();
  }

  public AppUserDto findDtoByUsername(String username) {
    return jooq.select(
      tAppUser.APP_USER_ID,
      tAppUser.USERNAME,
      tAppUser.PASSWORD,
      tAppUser.SAVE_SEARCH_ENABLED,
      tAppUser.ENABLED,
      tAppUser.LOCKED
    )
      .from(tAppUser)
      .where(tAppUser.USERNAME.eq(username))
      .fetchOneInto(AppUserDto.class);
  }
  
  public AppUserRecord findByUsername(String username) {
    return jooq.selectFrom(tAppUser)
      .where(tAppUser.USERNAME.eq(username))
      .fetchOne();
  }

  public List<AppUserDto> findAll() {
    return jooq.select(
      tAppUser.APP_USER_ID,
      tAppUser.USERNAME,
      tAppUser.PASSWORD,
      tAppUser.SAVE_SEARCH_ENABLED,
      tAppUser.ENABLED,
      tAppUser.LOCKED
    )
      .from(tAppUser)
      .fetchInto(AppUserDto.class);
  }
}
