package de.mett.mediacurator.securety;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import de.mett.mediacurator.securety.user.AppUserDto;
import de.mett.mediacurator.securety.user.AppUserRepository;

@Service("userDetailsService")
@Transactional
public class AppUserDetailService implements UserDetailsService {

  @Autowired
  private AppUserRepository userRepo;

  @Override
  public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {

    if (StringUtils.isEmptyOrWhitespace(username)) {
      throw new UsernameNotFoundException("No user found without username");
    }

    AppUserDto data = userRepo.findDtoByUsername(username);

    if (data == null) {
      throw new UsernameNotFoundException("No user found with username: " + username);
    }

    return data;
  }

}