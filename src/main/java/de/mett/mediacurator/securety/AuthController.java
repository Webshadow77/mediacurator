package de.mett.mediacurator.securety;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import de.mett.mediacurator.securety.user.AppUserDto;
import de.mett.mediacurator.securety.user.AppUserService;

@Controller
public class AuthController {

	@Autowired
	AppUserService userService;

	@GetMapping(value = "/login")
	public ModelAndView getLogin(Model model) {
		model.addAttribute("numberUsers",userService.findAll().size());
		return new ModelAndView("login-register/login");
	}

	@GetMapping(value = "/register")
	public ModelAndView getRegister(Model model) {
		return new ModelAndView("login-register/register");
	}

	@RequestMapping(value = "/register", method = { RequestMethod.POST, RequestMethod.PUT })
	public ModelAndView postRegister(Model model, @ModelAttribute("data") AppUserDto data) {
		userService.registerUser(data);
		return new ModelAndView("login-register/registersucsess");
	}
}
