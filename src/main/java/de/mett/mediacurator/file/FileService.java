package de.mett.mediacurator.file;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import de.mett.db.public_.tables.records.FileRecord;
import de.mett.db.public_.tables.records.TmpFileRecord;
import de.mett.mediacurator.config.HtmlTagBuilder;
import de.mett.mediacurator.filestorage.FileIoStorageService;
import de.mett.mediacurator.filestorage.FileType;
import de.mett.mediacurator.securety.user.AppUserDto;
import de.mett.mediacurator.tag.TagService;
import de.mett.mediacurator.tmp.TagDto;
import de.mett.mediacurator.tmp.TmpFileEditDTO;
import de.mett.mediacurator.tmp.TmpFileService;

@Service
public class FileService {

  @Autowired
  private FileRepository fileRepository;
  @Autowired
  private TagService tagService;
  @Autowired
  private TmpFileService tmpFileService;
  @Autowired
  private FileIoStorageService fileIoStorageService;

  @Transactional
  public void saveNewFile(TmpFileEditDTO dto) throws IOException {
    TmpFileRecord tmpFile = tmpFileService.findRawByID(dto.getId());
    if (tmpFile != null) {

      dto.addTag(tmpFile.getFileEnding());
      dto.setTags(dto.getTags().stream().filter(tag -> !ObjectUtils.isEmpty(tag)).collect(Collectors.toList()));
      List<TagDto> tagsInDb = tagService.findNewTagsAndSave(dto.getTags());
      FileRecord file = saveNewFileRefFromTmp(tmpFile);
      fileRepository.linkTags(file.getFileId(), tagsInDb);

      String newPath = fileIoStorageService
        .renameFiel(file.getFilePath(), genNewName(tmpFile, file.getFileId()));
      String destinationFolder = fileIoStorageService
        .getFolderPrefixByFileType(FileType.valueOf(tmpFile.getFileType()));
      newPath = fileIoStorageService.moveFileTo(newPath, destinationFolder);

      file.setFilePath(newPath);
      fileRepository.update(file);
    }
  }

  private String genNewName(TmpFileRecord oldFile, int uuid) {
    String oldName = oldFile.getFileName();
    return oldName.substring(0, oldName.lastIndexOf('.'))
      + "_" + uuid + oldFile.getFileEnding();
  }

  @Transactional
  public FileRecord saveNewFileRefFromTmp(TmpFileRecord tmpFile) {
    FileRecord file = fileRepository.newRecord();
    file.setFileEnding(tmpFile.getFileEnding());
    file.setFilePath(tmpFile.getFilePath());
    file.setFileType(tmpFile.getFileType());
    return fileRepository.save(file);
  }

  @Transactional
  public void deleteTagFromFile(String tagName) {
    fileRepository.deleteTagFromFile(tagName);
  }

  @Transactional
  public void deleteFile(Integer id) {
    String path = fileRepository.deleteFileFromDB(id);
    fileIoStorageService.removeFile(path);
  }

  @Transactional
  public void updateFile(FileEditDTO dto) {
    // Be save and push completely new tags to db
    List<TagDto> tags = tagService.findNewTagsAndSave(dto.getTags());
    fileRepository.delinkTagsFromFile(dto.getId());
    fileRepository.linkTags(dto.getId(), tags);
  }

  /**
   * Search for file by tag names. It will return the files that are taged with
   * minimum that tags. The file needs to be taged with all the tags we give or
   * more.
   *
   * @param searchText
   * @param pageNr
   * @param pageSize
   * @return
   */
  public List<FileDisplayDTO> searchByTags(List<String> tagNames, int pageNr, int pageSize, AppUserDto user) {
    if (user.isSaveSearchEnabled()) {
      return fillDisplayStringValue(
        fileRepository.findAllByMinimumTagsAndSaveSearch(
          tagNames,
          tagNames.size(),
          pageNr,
          pageSize
        )
      );
    } else {
      return fillDisplayStringValue(fileRepository.findAllByMinimumTags(tagNames, tagNames.size(), pageNr, pageSize));
    }
  }

  public List<FileDisplayDTO> findOftenViewedFiles(AppUserDto user) {
    if (user.isSaveSearchEnabled()) {
      return fillDisplayStringValue(fileRepository.findAllByOrderByUpdatedAtDescWithSaveSearch(0, 20));
    } else {
      return fillDisplayStringValue(fileRepository.findAllByOrderByUpdatedAtDesc(0, 20));
    }
  }

  public FileDisplayDTO findById(Integer id) {
    FileDisplayDTO dto = fileRepository.findById(id);
    dto.setFullSourceDisplayString(genDisplayStrings(dto));
    return dto;
  }

  @Transactional
  public void updateLastViewed(FileDisplayDTO file) {
    fileRepository.touch(file.getId());
  }

  public List<FileDisplayDTO> findByTag(PageRequest page, String tagName, AppUserDto user) {
    if (user.isSaveSearchEnabled()) {
      return fillDisplayStringValue(fileRepository.findByTagsNameWithSaveSearch(page, tagName));
    } else {
      return fillDisplayStringValue(fileRepository.findByTagsName(page, tagName));
    }
  }

  public List<FileDisplayDTO> findRandomFiles(AppUserDto user) {
    if (user.isSaveSearchEnabled()) {
      return fillDisplayStringValue(fileRepository.findAllByOrderByRandomWithSaveSearch(0, 20));
    } else {
      return fillDisplayStringValue(fileRepository.findAllByOrderByRandom(0, 10));
    }
  }

  public List<FileDisplayDTO> findNewestFiles(AppUserDto user) {
    if (user.isSaveSearchEnabled()) {
      return fillDisplayStringValue(fileRepository.findAllByOrderByCreatedAtDescWithSaveSearch(0, 20));
    } else {
      return fillDisplayStringValue(fileRepository.findAllByOrderByCreatedAtDesc(0, 20));
    }
  }

  public List<FileDisplayDTO> fillDisplayStringValue(List<FileDisplayDTO> dtoList) {
    for (FileDisplayDTO dto : dtoList) {
      dto.setPreviewDisplayString(genDisplayPreviewStrings(dto));
    }
    return dtoList;
  }

  public String genDisplayPreviewStrings(FileDisplayDTO file) {
    String path = file.getPath().replace('\\', '/');
    return HtmlTagBuilder.genDisplayPreviewStrings(path, FileType.valueOf(file.getType()));
  }

  public String genDisplayStrings(FileDisplayDTO file) {
    String path = file.getPath().replace('\\', '/');
    return HtmlTagBuilder.genDisplayStrings(path, FileType.valueOf(file.getType()));
  }
}
