package de.mett.mediacurator.file;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sebastian.mett
 *
 */
public class FileEditDTO {

	private Integer id;
	private List<String> tags = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public void addTag(String tagName) {
		tags.add(tagName);
	}

}
