package de.mett.mediacurator.file;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import de.mett.mediacurator.securety.user.AppUserDto;
import de.mett.mediacurator.securety.user.AppUserService;

@Controller
@RequestMapping("/files")
public class FileController {

  @Autowired
  private FileService fileService;
  @Autowired
  private AppUserService userService;

  @GetMapping("/explore")
  public ModelAndView getExplorepage(Model model, Principal principal) {

    model.addAttribute("random_files", fileService.findRandomFiles(getUser(principal)));
    model.addAttribute("newest_files", fileService.findNewestFiles(getUser(principal)));

    model.addAttribute("pageroot", "files");
    return new ModelAndView("files/files-overview");
  }

  @GetMapping("singel")
  public ModelAndView getSingleFile(@RequestParam Integer uuid, Model model) {
    FileDisplayDTO file = fileService.findById(uuid);
    fileService.updateLastViewed(file);
    model.addAttribute("file", file);
    model.addAttribute("pageroot", "files");
    return new ModelAndView("files/file-single-view");
  }

  @GetMapping("by-tag")
  public ModelAndView getByTag(@RequestParam String tag, Model model, Principal principal) {
    model.addAttribute("tag", tag);
    model.addAttribute("files", fileService.findByTag(PageRequest.of(0, 21), tag, getUser(principal)));
    model.addAttribute("pageroot", "files");
    return new ModelAndView("files/file-list-by-tag");
  }

  @PostMapping("delete")
  @ResponseBody
  public ResponseEntity<Object> deleteFile(@RequestParam Integer id) {
    fileService.deleteFile(id);
    return ResponseEntity.ok().build();
  }

  @GetMapping("edit")
  public ModelAndView getEditPAge(@RequestParam Integer uuid, Model model) {
    model.addAttribute("file", fileService.findById(uuid));
    model.addAttribute("pageroot", "files");
    return new ModelAndView("files/file-edit");
  }

	@PostMapping("/update")
	public RedirectView persistTmpFile(Model model, FileEditDTO dto) {
		fileService.updateFile(dto);
		return new RedirectView("/files/singel?uuid=" + dto.getId());
	}

  @GetMapping("searchrequest")
  @ResponseBody
  public List<FileDisplayDTO> search(
    @RequestParam List<String> tags,
    @RequestParam(required = false, defaultValue = "0") int pageNr,
    @RequestParam(required = false, defaultValue = "20") int pageSize,
    Principal principal
  ) {
    if (tags.isEmpty()) {
			return fileService.findOftenViewedFiles(getUser(principal));
    } else {
			return fileService.searchByTags(tags, pageNr, pageSize, getUser(principal));
    }
  }

  public AppUserDto getUser(Principal principal) {
    return userService.getUser(principal.getName());
  }

}
