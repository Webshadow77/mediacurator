package de.mett.mediacurator.file;

import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.listAgg;
import static org.jooq.impl.DSL.rand;
import static org.jooq.impl.DSL.select;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.jooq.DSLContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import de.mett.db.public_.Sequences;
import de.mett.db.public_.tables.BlacklistElement;
import de.mett.db.public_.tables.File;
import de.mett.db.public_.tables.File2tag;
import de.mett.db.public_.tables.Tag;
import de.mett.db.public_.tables.records.FileRecord;
import de.mett.mediacurator.tmp.TagDto;

@Repository
public class FileRepository {

  private static final File tFile = File.FILE;
  private static final File2tag tFile2tag = File2tag.FILE2TAG;
  private static final Tag tTag = Tag.TAG;
  private static final BlacklistElement tBlacklistElement = BlacklistElement.BLACKLIST_ELEMENT;

  private final DSLContext jooq;

  public FileRepository(DSLContext jooq) {
    this.jooq = jooq;
  }

  public FileRecord newRecord() {
    return jooq.newRecord(tFile);
  }

  @Transactional
  public FileRecord save(FileRecord newRecord) {
    Integer id = jooq.nextval(Sequences.SEQ_FILE).intValue();
    newRecord.setFileId(id);
    newRecord.store();
    return jooq.selectFrom(tFile).where(tFile.FILE_ID.eq(id)).fetchOne();
  }

  @Transactional
  public void touch(Integer fileId) {
    jooq.update(tFile)
      .set(tFile.UPDATED_AT, LocalDateTime.now())
      .where(tFile.FILE_ID.eq(fileId))
      .execute();
  }

  @Transactional
  public void update(FileRecord exsistingRecord) {
    exsistingRecord.update();
  }

  @Transactional
  public void linkTags(Integer fileId, List<TagDto> exsistingTags) {
    for (TagDto tag : exsistingTags) {
      jooq.insertInto(tFile2tag, tFile2tag.FILE_ID, tFile2tag.TAG_ID)
        .values(fileId, tag.getTagId())
        .execute();
    }
  }

  @Transactional
  public void delinkTagsFromFile(Integer fileId) {
    jooq.deleteFrom(tFile2tag)
      .where(tFile2tag.FILE_ID.eq(fileId))
      .execute();
  }

  @Transactional
  public void deleteTagFromFile(String tagName) {
    jooq.delete(tFile2tag)
      .where(
        tFile2tag.TAG_ID.in(
          select(tTag.TAG_ID)
            .from(tTag)
            .where(tTag.TAG_NAME.eq(tagName))
        )
      )
      .execute();
  }

  @Transactional
  public String deleteFileFromDB(Integer id) {
    delinkTagsFromFile(id);
    FileRecord file = jooq.selectFrom(tFile).where(tFile.FILE_ID.eq(id)).fetchOne();
    String path = file.getFilePath();
    file.delete();
    return path;
  }

  public FileDisplayDTO findById(Integer id) {
    return jooq.select(
      tFile.FILE_ID.as("id"),
      tFile.FILE_ENDING.as("ending"),
      tFile.FILE_PATH.as("path"),
      tFile.FILE_TYPE.as("type"),
      field(
        select(listAgg(tTag.TAG_NAME, "$").withinGroupOrderBy(tTag.TAG_NAME))
          .from(tTag)
          .leftJoin(tFile2tag).on(tTag.TAG_ID.eq(tFile2tag.TAG_ID))
          .where(tFile2tag.FILE_ID.eq(id))
      ).as("tags")
    )
      .from(tFile)
      .where(tFile.FILE_ID.eq(id))
      .fetchOneInto(FileDisplayDTO.class);
  }

  // ----------------- SELECT SECTION
  // ---------------------------------------------------------

  public List<FileDisplayDTO> findByTagsNameWithSaveSearch(PageRequest pageRequest, String name) {
    return jooq.select(
      tFile.FILE_ID.as("id"),
      tFile.FILE_ENDING.as("ending"),
      tFile.FILE_PATH.as("path"),
      tFile.FILE_TYPE.as("type")
    )
      .from(tFile)
      .where(
        tFile.FILE_ID.in(
          select(tFile2tag.FILE_ID)
            .from(tFile2tag)
            .rightJoin(tTag)
            .on(tFile2tag.TAG_ID.eq(tTag.TAG_ID))
            .and(tTag.TAG_NAME.eq(name))
        )
      )
      .and(
        tFile.FILE_ID.notIn(
          select(tFile.FILE_ID)
            .from(tFile)
            .leftJoin(tFile2tag).on(tFile2tag.FILE_ID.eq(tFile.FILE_ID))
            .leftJoin(tTag).on(tTag.TAG_ID.eq(tFile2tag.TAG_ID))
            .rightJoin(tBlacklistElement).on(tBlacklistElement.TAG_ID.eq(tTag.TAG_ID))
        )
      )
      .offset(pageRequest.getPageNumber() * pageRequest.getPageSize())
      .limit(pageRequest.getPageSize())
      .fetchInto(FileDisplayDTO.class);
  }

  public List<FileDisplayDTO> findByTagsName(PageRequest pageRequest, String name) {
    return jooq.select(
      tFile.FILE_ID.as("id"),
      tFile.FILE_ENDING.as("ending"),
      tFile.FILE_PATH.as("path"),
      tFile.FILE_TYPE.as("type")
    )
      .from(tFile)
      .where(
        tFile.FILE_ID.in(
          select(tFile2tag.FILE_ID)
            .from(tFile2tag)
            .rightJoin(tTag)
            .on(tFile2tag.TAG_ID.eq(tTag.TAG_ID))
            .and(tTag.TAG_NAME.eq(name))
        )
      )
      .offset(pageRequest.getPageNumber() * pageRequest.getPageSize())
      .limit(pageRequest.getPageSize())
      .fetchInto(FileDisplayDTO.class);
  }

  public List<FileDisplayDTO> findAllByOrderByCreatedAtDesc(int pageNr, int pageSize) {
    return jooq.select(
      tFile.FILE_ID.as("id"),
      tFile.FILE_ENDING.as("ending"),
      tFile.FILE_PATH.as("path"),
      tFile.FILE_TYPE.as("type")
    )
      .from(tFile)
      .orderBy(tFile.CREATED_AT.desc())
      .offset(pageNr * pageSize)
      .limit(pageSize)
      .fetchInto(FileDisplayDTO.class);
  }

  public List<FileDisplayDTO> findAllByOrderByCreatedAtDescWithSaveSearch(int pageNr, int pageSize) {
    return jooq.select(
      tFile.FILE_ID.as("id"),
      tFile.FILE_ENDING.as("ending"),
      tFile.FILE_PATH.as("path"),
      tFile.FILE_TYPE.as("type")
    )
      .from(tFile)
      .where(
        tFile.FILE_ID.notIn(
          select(tFile.FILE_ID)
            .from(tFile)
            .leftJoin(tFile2tag).on(tFile2tag.FILE_ID.eq(tFile.FILE_ID))
            .leftJoin(tTag).on(tTag.TAG_ID.eq(tFile2tag.TAG_ID))
            .rightJoin(tBlacklistElement).on(tBlacklistElement.TAG_ID.eq(tTag.TAG_ID))
        )
      )
      .orderBy(tFile.CREATED_AT.desc())
      .offset(pageNr * pageSize)
      .limit(pageSize)
      .fetchInto(FileDisplayDTO.class);
  }

  public List<FileDisplayDTO> findAllByOrderByUpdatedAtDesc(int pageNr, int pageSize) {
    return jooq.select(
      tFile.FILE_ID.as("id"),
      tFile.FILE_ENDING.as("ending"),
      tFile.FILE_PATH.as("path"),
      tFile.FILE_TYPE.as("type")
    )
      .from(tFile)
      .orderBy(tFile.UPDATED_AT.desc())
      .offset(pageNr * pageSize)
      .limit(pageSize)
      .fetchInto(FileDisplayDTO.class);
  }

  public List<FileDisplayDTO> findAllByOrderByUpdatedAtDescWithSaveSearch(int pageNr, int pageSize) {
    return jooq.select(
      tFile.FILE_ID.as("id"),
      tFile.FILE_ENDING.as("ending"),
      tFile.FILE_PATH.as("path"),
      tFile.FILE_TYPE.as("type")
    )
      .from(tFile)
      .where(
        tFile.FILE_ID.notIn(
          select(tFile.FILE_ID)
            .from(tFile)
            .leftJoin(tFile2tag).on(tFile2tag.FILE_ID.eq(tFile.FILE_ID))
            .leftJoin(tTag).on(tTag.TAG_ID.eq(tFile2tag.TAG_ID))
            .rightJoin(tBlacklistElement).on(tBlacklistElement.TAG_ID.eq(tTag.TAG_ID))
        )
      )
      .orderBy(tFile.UPDATED_AT.desc())
      .offset(pageNr * pageSize)
      .limit(pageSize)
      .fetchInto(FileDisplayDTO.class);
  }

//  public Page<File> findAllByOrderByUpdatedAtAsc(Pageable pageRequest) {
//
//  }

  /*
   * "select * from file f \r\n" + "	where f.uuid not in (\r\n" +
   * "		select uuid from file f1\r\n" + "		left join file_tags ft on\r\n" +
   * "			f1.uuid = ft.files_uuid\r\n" + "		left join tag tag on\r\n" +
   * "			ft.tags_id = tag.id\r\n" +
   * "		right join blacklist_element bl on\r\n" + "			tag.id = bl.tag_id \r\n"
   * + "	)\r\n" +
   * "order by f.last_toucht offset :pageOffset rows fetch next :pageSize rows only"
   */
//  public List<File> findAllByOrderByUpdatedAtAscWithSaveSearch(
//    @Param("pageOffset") int pageOffset,
//    @Param("pageSize") int pageSize
//  ) {
//
//  }

//  public Page<File> findAllByTagsNameIn(Pageable pageRequest, List<String> tagnames) {
//
//  }

  public List<FileDisplayDTO> findAllByOrderByRandom(int pageNr, int pageSize) {
    return jooq.select(
      tFile.FILE_ID.as("id"),
      tFile.FILE_ENDING.as("ending"),
      tFile.FILE_PATH.as("path"),
      tFile.FILE_TYPE.as("type")
    )
      .from(tFile)
      .orderBy(rand())
      .offset(pageNr * pageSize)
      .limit(pageSize)
      .fetchInto(FileDisplayDTO.class);
  }

  public List<FileDisplayDTO> findAllByOrderByRandomWithSaveSearch(int pageNr, int pageSize) {
    return jooq.select(
      tFile.FILE_ID.as("id"),
      tFile.FILE_ENDING.as("ending"),
      tFile.FILE_PATH.as("path"),
      tFile.FILE_TYPE.as("type")
    )
      .from(tFile)
      .where(
        tFile.FILE_ID.notIn(
          select(tFile.FILE_ID)
            .from(tFile)
            .leftJoin(tFile2tag).on(tFile2tag.FILE_ID.eq(tFile.FILE_ID))
            .leftJoin(tTag).on(tTag.TAG_ID.eq(tFile2tag.TAG_ID))
            .rightJoin(tBlacklistElement).on(tBlacklistElement.TAG_ID.eq(tTag.TAG_ID))
        )
      )
      .orderBy(rand())
      .offset(pageNr * pageSize)
      .limit(pageSize)
      .fetchInto(FileDisplayDTO.class);
  }

  /**
   * This is the magic
   * 
   * @param pageRequest
   * @param tagnames
   * @param tagcount
   * @return
   */
  public List<FileDisplayDTO> findAllByMinimumTags(
    List<String> tagnames,
    int tagcount,
    int pageNr,
    int pageSize
  ) {
    return jooq.select(
      tFile.FILE_ID.as("id"),
      tFile.FILE_ENDING.as("ending"),
      tFile.FILE_PATH.as("path"),
      tFile.FILE_TYPE.as("type")
    ).from(tFile)
      .where(
        tFile.FILE_ID.in(
          select(tFile2tag.FILE_ID)
            .from(tFile2tag)
            .rightJoin(tTag)
            .on(tFile2tag.TAG_ID.eq(tTag.TAG_ID))
            .where(tTag.TAG_NAME.in(tagnames))
            .groupBy(tFile2tag.FILE_ID).having(count(tFile2tag.FILE_ID).ge(tagcount))
        )
      )
      .offset(pageNr * pageSize)
      .limit(pageSize)
      .fetchInto(FileDisplayDTO.class);
  }

  /**
   * This is the magic with savesearch
   * 
   * @param pageRequest
   * @param tagnames
   * @param tagcount
   * @return
   */
  public List<FileDisplayDTO> findAllByMinimumTagsAndSaveSearch(
    List<String> tagnames,
    int tagcount,
    int pageNr,
    int pageSize
  ) {
    return jooq.select(
      tFile.FILE_ID.as("id"),
      tFile.FILE_ENDING.as("ending"),
      tFile.FILE_PATH.as("path"),
      tFile.FILE_TYPE.as("type")
    ).from(tFile)
      .where(
        tFile.FILE_ID.in(
          select(tFile2tag.FILE_ID)
            .from(tFile2tag)
            .rightJoin(tTag)
            .on(tFile2tag.TAG_ID.eq(tTag.TAG_ID))
            .where(tTag.TAG_NAME.in(tagnames))
            .groupBy(tFile2tag.FILE_ID).having(count(tFile2tag.FILE_ID).ge(tagcount))
        )
      )
      .and(
        tFile.FILE_ID.notIn(
          select(tFile.FILE_ID)
            .from(tFile)
            .leftJoin(tFile2tag).on(tFile2tag.FILE_ID.eq(tFile.FILE_ID))
            .leftJoin(tTag).on(tTag.TAG_ID.eq(tFile2tag.TAG_ID))
            .rightJoin(tBlacklistElement).on(tBlacklistElement.TAG_ID.eq(tTag.TAG_ID))
        )
      )
      .offset(pageNr * pageSize)
      .limit(pageSize)
      .fetchInto(FileDisplayDTO.class);

  }
}