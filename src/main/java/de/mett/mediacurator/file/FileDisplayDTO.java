package de.mett.mediacurator.file;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author sme
 *
 */
public class FileDisplayDTO {

  private Integer id;
  private String ending;
  private String path;
  private String type;
  private String previewDisplayString;
  private String fullSourceDisplayString;
  private List<String> tags;

  public FileDisplayDTO(Integer id, String ending, String path, String type) {
    this.id = id;
    this.ending = ending;
    this.path = path;
    this.type = type;
  }
  
  public FileDisplayDTO(Integer id, String ending, String path, String type, String tags) {
    this.id = id;
    this.ending = ending;
    this.path = path;
    this.type = type;
    setTags(tags);
  }

  public String getEnding() {
    return ending;
  }

  public void setEnding(String ending) {
    this.ending = ending;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getPreviewDisplayString() {
    return previewDisplayString;
  }

  public void setPreviewDisplayString(String previewDisplayString) {
    this.previewDisplayString = previewDisplayString;
  }

  public String getFullSourceDisplayString() {
    return fullSourceDisplayString;
  }

  public void setFullSourceDisplayString(String fullSourceDisplayString) {
    this.fullSourceDisplayString = fullSourceDisplayString;
  }

  public List<String> getTags() {
    return tags;
  }

  public void setTags(String tags) {
    this.tags = Arrays.asList(tags.split("\\$"));
  }

  public void setTagList(List<String> tags) {
    this.tags = tags;
  }

}
