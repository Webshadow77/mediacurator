package de.mett.mediacurator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import de.mett.mediacurator.filestorage.FileIoWatcher;
import de.mett.mediacurator.tmp.TmpFileService;

/**
 * 
 * @author Sebastian Mett
 *
 */
@SpringBootApplication
public class App extends SpringBootServletInitializer {

  @Bean
  public CommonsMultipartResolver filterMultipartResolver() {
    return new CommonsMultipartResolver();
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(App.class);
  }

  public static void main(String[] args) {
    ConfigurableApplicationContext context = SpringApplication.run(App.class, args);
		TmpFileService tmpFileService = context.getBean(TmpFileService.class);
		tmpFileService.updateTmpFileFolderRegistry();
		FileIoWatcher fileIoWatcher = context.getBean(FileIoWatcher.class);
		fileIoWatcher.start();
  }
}
