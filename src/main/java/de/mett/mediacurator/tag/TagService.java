package de.mett.mediacurator.tag;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import de.mett.db.public_.tables.records.BlacklistElementRecord;
import de.mett.db.public_.tables.records.TagRecord;
import de.mett.mediacurator.securety.user.AppUserDto;
import de.mett.mediacurator.tmp.TagDto;

@Service
public class TagService {

  @Autowired
  private TagRepository tagRepository;
  @Autowired
  private BlacklistRepository blacklistRepository;

  /**
   * Saves all the tags that are not in the repo and returns all the tags that are
   * in the names that are given into the method
   * 
   * @param tagNames
   * @return
   */
  @Transactional
  public List<TagDto> findNewTagsAndSave(Collection<String> tagNames) {
    tagNames = tagNames.stream().filter(tag -> tag != null).collect(Collectors.toList());
    List<String> newTagsToInsert = findTagsNotInRepo(tagNames);
    tagRepository.bulkInsertNew(newTagsToInsert);
    return tagRepository.findByNameIn(new ArrayList<>(tagNames));
  }

  @Transactional
  public void addNewTag(String tagName) {
    tagRepository.insertNew(tagName);
  }

  @Transactional
  public void updateTag(String originalName, String updatedName, boolean blacklistUpdate) {
    TagRecord dbTagRecord = tagRepository.findByName(originalName);
    dbTagRecord.setTagName(updatedName);
    tagRepository.update(dbTagRecord);

    BlacklistElementRecord marking = blacklistRepository.findByTagName(updatedName);
    if (blacklistUpdate) {
      if (marking == null) {
        blacklistRepository.markAsBlacklistedByTagId(dbTagRecord.getTagId());
      }
    } else {
      blacklistRepository.deleteByTagId(dbTagRecord.getTagId());
    }
  }

  public boolean isBlacklisted(String name) {
    return tagRepository.isBlacklisted(name);
  }

  @Transactional
  public void deleteTag(String name) {
    TagRecord tag = tagRepository.findByName(name);
    tagRepository.delete(tag);
  }

  public List<String> findBySearchSuggestion(String searchText, AppUserDto user) {
    return tagRepository.findFirstXByNameContains(searchText, 5);
  }

  public List<String> findAllTagnames(int page, int resultesPerPage) {
    Pageable pageRequest = PageRequest.of(page, resultesPerPage, Sort.Direction.ASC, "name");
    return tagRepository.findAllTagnames(pageRequest);
  }

  public List<String> findTagsNotInRepo(Collection<String> externalTags) {
    List<String> dbTags = tagRepository.findByNameIn(new ArrayList<>(externalTags)).stream().map(TagDto::getTagName)
      .collect(Collectors.toList());
    return externalTags.stream().filter(extern -> !dbTags.contains(extern)).collect(Collectors.toList());
  }
}
