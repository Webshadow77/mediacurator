package de.mett.mediacurator.tag;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import de.mett.mediacurator.file.FileService;
import de.mett.mediacurator.securety.user.AppUserDto;
import de.mett.mediacurator.securety.user.AppUserService;

@Controller
@RequestMapping("/tags")
public class TagController {

  @Autowired
  private TagService tagService;
  @Autowired
  private FileService fileService;
  @Autowired
  private AppUserService userService;

  @GetMapping(value = "/overview")
  public ModelAndView getTagOverview(Model model) {
    model.addAttribute("pageroot", "tags");
    return new ModelAndView("tags/tag-overview");
  }

  @GetMapping(value = "/optins")
  public ModelAndView getTagoptions(Model model) {
    model.addAttribute("pageroot", "tags");
    return new ModelAndView("tags/tag-options");
  }

  @GetMapping(value = "/edit")
  public ModelAndView getTagEditPage(Model model, @RequestParam String name) {
    model.addAttribute("pageroot", "tags");
    model.addAttribute("tagname", name);
    model.addAttribute("blacklist", tagService.isBlacklisted(name));
    return new ModelAndView("tags/tag-edit");
  }
  
  @GetMapping(value = "/list")
  @ResponseBody
  public List<String> getTagList(@RequestParam("page") int pageNr, @RequestParam("respp") int resultesPerPage) {
    return tagService.findAllTagnames(pageNr, resultesPerPage);
  }
  
  @GetMapping(value = "/suggestion")
  @ResponseBody
  public List<String> getTagSearchList(@RequestParam String searchText, Principal principal) {
    return tagService.findBySearchSuggestion(searchText, getUser(principal));
  }

  @PostMapping("/add")
  @ResponseBody
  public ResponseEntity<Object> addTag(
    @RequestParam String name
  ) {
    tagService.addNewTag(name);
    return ResponseEntity.ok().build();
  }

  @PostMapping("/edit")
  @ResponseBody
  public ResponseEntity<Object> addTag(
    @RequestParam String originalname,
    @RequestParam String updatedname,
    @RequestParam(defaultValue = "false", required = false) boolean blacklist
  ) {
    tagService.updateTag(originalname, updatedname, blacklist);
    return ResponseEntity.ok().build();
  }

  @PostMapping("/delite")
  @ResponseBody
  public ResponseEntity<Object> deliteTag(@RequestParam String name) {
    fileService.deleteTagFromFile(name);
    tagService.deleteTag(name);
    return ResponseEntity.ok().build();
  }

  public AppUserDto getUser(Principal principal) {
    return userService.getUser(principal.getName());
  }
}
