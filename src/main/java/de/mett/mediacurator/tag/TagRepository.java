package de.mett.mediacurator.tag;

import static org.jooq.impl.DSL.length;
import static org.jooq.impl.DSL.position;
import static org.jooq.impl.DSL.select;
import static org.jooq.impl.DSL.when;

import java.util.List;

import javax.transaction.Transactional;

import org.jooq.DSLContext;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import de.mett.db.public_.tables.BlacklistElement;
import de.mett.db.public_.tables.Tag;
import de.mett.db.public_.tables.records.TagRecord;
import de.mett.mediacurator.tmp.TagDto;

@Repository
public class TagRepository {

  private static final Tag tTag = Tag.TAG;
  private static final BlacklistElement tBlacklistElement = BlacklistElement.BLACKLIST_ELEMENT;

  private final DSLContext jooq;

  public TagRepository(DSLContext jooq) {
    this.jooq = jooq;
  }

  @Transactional
  public void bulkInsertNew(List<String> tagName) {
    for (String tag : tagName) {
      jooq.insertInto(tTag, tTag.TAG_NAME)
        .values(tag)
        .execute();
    }
  }

  @Transactional
  public void insertNew(String tagName) {
    TagRecord tag = jooq.newRecord(tTag);
    tag.setTagName(tagName);
    tag.store();
  }

  /**
   * Must be correct initialisiest jooq record
   * 
   * @param dbRecord
   */
  @Transactional
  public void update(TagRecord dbRecord) {
    dbRecord.update();
  }

  public boolean isBlacklisted(String name) {
    return jooq.select(when(tBlacklistElement.TAG_ID.isNotNull(), true).otherwise(false))
      .from(tTag)
      .leftJoin(tBlacklistElement)
      .on(tTag.TAG_ID.eq(tBlacklistElement.TAG_ID))
      .where(tTag.TAG_NAME.eq(name))
      .fetchOneInto(Boolean.class);
  }

  public List<String> findAllTagnames(Pageable pageRequest) {
    return jooq.select(tTag.TAG_NAME)
      .from(tTag)
      .offset(pageRequest.getPageNumber() * pageRequest.getPageSize())
      .limit(pageRequest.getPageSize())
      .fetchInto(String.class);
  }

  public TagRecord findByName(String name) {
    return jooq.selectFrom(tTag)
      .where(tTag.TAG_NAME.eq(name))
      .fetchOne();
  }

  public List<TagDto> findByNameIn(List<String> names) {
    return jooq.select(
      tTag.TAG_ID,
      tTag.TAG_NAME
    ).from(tTag)
      .where(tTag.TAG_NAME.in(names))
      .fetchInto(TagDto.class);
  }

  public List<String> findFirstXByNameContains(String name, int numberOfReturns) {
    return jooq.select(tTag.TAG_NAME)
      .from(tTag)
      .where(tTag.TAG_NAME.contains(name))
      .orderBy(position(tTag.TAG_NAME, name), length(tTag.TAG_NAME))
      .limit(numberOfReturns)
      .fetchInto(String.class);
  }

  public void delete(TagRecord name) {
    name.delete();
  }
}
