package de.mett.mediacurator.tag;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;

import de.mett.db.public_.tables.BlacklistElement;
import de.mett.db.public_.tables.Tag;
import de.mett.db.public_.tables.records.BlacklistElementRecord;

@Repository
public class BlacklistRepository {

  private static final BlacklistElement tBlacklistElement = BlacklistElement.BLACKLIST_ELEMENT;
  private static final Tag tTag = Tag.TAG;

  private final DSLContext jooq;

  public BlacklistRepository(DSLContext jooq) {
    this.jooq = jooq;
  }

  /**
   * Find a {@link BlacklistElement} by its tag name
   * 
   * @param content
   * @return
   */
  public BlacklistElementRecord findByTagName(String name) {
    return jooq.select(tBlacklistElement.asterisk())
      .from(tBlacklistElement)
      .leftJoin(tTag)
      .on(tBlacklistElement.TAG_ID.eq(tTag.TAG_ID))
      .and(tTag.TAG_NAME.eq(name))
      .where(tTag.TAG_ID.isNotNull())
      .fetchOneInto(BlacklistElementRecord.class);
  }

  public void deleteByTagId(Integer tagId) {
    jooq.delete(tBlacklistElement)
      .where(tBlacklistElement.TAG_ID.eq(tagId))
      .execute();
  }

  public void markAsBlacklistedByTagId(Integer tagId) {
    jooq.insertInto(tBlacklistElement, tBlacklistElement.TAG_ID)
      .values(tagId)
      .execute();
  }
}
