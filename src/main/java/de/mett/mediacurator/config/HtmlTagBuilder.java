package de.mett.mediacurator.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import de.mett.mediacurator.filestorage.FileType;

public class HtmlTagBuilder {

  private HtmlTagBuilder() {

  }

  public static String genDisplayPreviewStrings(String path, FileType fileType) {
    switch (fileType) {
      case IMAGE:
        return genPreviewStringForImages(path);
      case TEXT:
        return genPreviewStringForText();
      case VIDEO:
        return genPreviewStringForVideo(path);
      case PDF:
        return genPreviewStringForPDF();
      case AUDIO:
        return genPreviewStringForAudio(path);
      case LINK:
        return genPreviewStringForLink(path);
      case OTHER:
        return genPreviewStringForOther(path);
      default:
        return genPreviewStringForOther(path);
    }
  }

  private static String genPreviewStringForImages(String path) {
    return "<img class=\"img-responsive img-rounded\" src=\"" + path + "\">";
  }

  private static String genPreviewStringForText() {
    return "<img class=\"img-responsive img-rounded\" src=\"/imgs/txt-file-icon-64765.png\">";
  }

  private static String genPreviewStringForVideo(String path) {
    return "<div class=\"embed-responsive embed-responsive-16by9\">\r\n" + "<video src=\"" + path + "\"></video>"
      + "     </div>";
  }

  private static String genPreviewStringForPDF() {
    return "<img class=\"img-responsive img-rounded\" src=\"/imgs/pdfIcon.png\">";
  }

  private static String genPreviewStringForAudio(String path) {
    return "<div class=\"embed-responsive embed-responsive-16by9\" style=\"background-color: #fff0b3\">\r\n"
      + "       <audio controls=\"controls\">\r\n"
      + "         Your browser does not support the <code>audio</code> element.\r\n"
      + "         <source src=\"" + path + "\">\r\n" + "        </audio>\r\n"
      + "     </div>";
  }

  private static String genPreviewStringForLink(String path) {
    return "there is at that moment no html that we display for links";
  }

  private static String genPreviewStringForOther(String path) {
    return "<a href=\"" + path
      + "\" target=\"_blank\"><img class=\"img-responsive img-rounded\" src=\"/imgs/Sed-24-512.png\"></a>";
  }

  public static String genDisplayStrings(String path, FileType fileType) {
    switch (fileType) {
      case IMAGE:
        return genDisplayStringForImages(path);
      case TEXT:
        return genDisplayStringForText(path);
      case VIDEO:
        return genDisplayStringForVideo(path);
      case PDF:
        return genDisplayStringForPDF(path);
      case AUDIO:
        return genDisplayStringForAudio(path);
      case LINK:
        return genPreviewStringForLink(path);
      case OTHER:
        return genPreviewStringForOther(path);
      default:
        return genPreviewStringForOther(path);
    }
  }

  private static String genDisplayStringForImages(String path) {
    return "<a href=\"" + path + "\" target=\"_blank\" tabIndex=\"-1\"><img class=\"img-responsive img-rounded\" src=\""
      + path + "\"></a>";
  }

  private static String genDisplayStringForText(String path) {
    path = path.substring(1).replace('/', '\\');
    String text = null;
    try {
      text = new String(Files.readAllBytes(Paths.get(path)));
    } catch (IOException e) {
      e.printStackTrace();
    }
    text = "<p>" + text + "</p>";
    text = text.replaceAll("\n", "<br>");
    return text;
  }

  private static String genDisplayStringForVideo(String path) {
    return "<video width=\"100%\" height=\"auto\" controls class=\"embed-responsive-item\" tabIndex=\"-1\">\r\n"
      + "         <source src=\"" + path + "\" type=\"video/mp4\">\r\n"
      + "         Your browser does not support the video tag.</video>";
  }

  private static String genDisplayStringForPDF(String path) {
    return "<a href=\"" + path
      + "\" target=\"_blank\"><img class=\"img-responsive img-rounded\" src=\"/imgs/pdfIcon.png\"></a>";
  }

  private static String genDisplayStringForAudio(String path) {
    return "<audio controls=\"controls\">\r\n"
      + "         Your browser does not support the <code>audio</code> element.\r\n"
      + "         <source src=\"" + path + "\">\r\n" + "        </audio>\r\n";
  }
}
