package de.mett.mediacurator.filestorage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import de.mett.mediacurator.tmp.TmpFileDisplayDTO;

@Service
public class FileIoStorageService {

  public final File fileStorageBaseLocation;
  public final File incommingFileLocation;
  public final File imageFileLocation;
  public final File textFileLocation;
  public final File videoFileLocation;
  public final File pdfFileLocation;
  public final File audioFileLocation;
  public final File linksFileLocation;
  public final File otherFileLocation;

  private String lastRenameFileName = "";

  @Autowired
  public FileIoStorageService() {
    this.fileStorageBaseLocation = new File(FileCommens.FOLDER_BATH_PATH);
    this.incommingFileLocation = new File(
      FileCommens.INCOMMING_FILES_URL_PREFIX
    );
    this.imageFileLocation = new File(FileCommens.IMAGE_URL_PREFIX);
    this.textFileLocation = new File(FileCommens.TEXT_URL_PREFIX);
    this.videoFileLocation = new File(FileCommens.VIDEO_URL_PREFIX);
    this.pdfFileLocation = new File(FileCommens.PDF_URL_PREFIX);
    this.audioFileLocation = new File(FileCommens.AUDIO_URL_PREFIX);
    this.linksFileLocation = new File(FileCommens.LINKS_URL_PREFIX);
    this.otherFileLocation = new File(FileCommens.OTHER_URL_PREFIX);
    try {
      Files.createDirectories(this.fileStorageBaseLocation.toPath());
      Files.createDirectories(this.incommingFileLocation.toPath());
      Files.createDirectories(this.imageFileLocation.toPath());
      Files.createDirectories(this.textFileLocation.toPath());
      Files.createDirectories(this.videoFileLocation.toPath());
      Files.createDirectories(this.pdfFileLocation.toPath());
      Files.createDirectories(this.audioFileLocation.toPath());
      Files.createDirectories(this.linksFileLocation.toPath());
      Files.createDirectories(this.otherFileLocation.toPath());
    } catch (IOException ex) {
      System.out.println(
        "Could not create the directory where the uploaded files will be stored."
          + ex
      );
    }
  }

  public File[] getAllFilesInIncommingFolder() {
    File parentDirectory = new File(FileCommens.INCOMMING_FILES_URL_PREFIX);
    return parentDirectory.listFiles();
  }

  public File storeFile(MultipartFile file) {
    // Normalize file name
    String fileName = StringUtils.cleanPath(file.getOriginalFilename());
    try {
      // Check if the file's name contains invalid characters
      if (fileName.contains("..")) {
        System.out.println(
          "Sorry! Filename contains invalid path sequence "
            + fileName
        );
      }
      // Copy file to the target location (Replacing existing file with
      // the same name)
      Path targetLocation = (new File(
        FileCommens.INCOMMING_FILES_URL_PREFIX + "/" + fileName
      )).toPath();
      Files.copy(
        file.getInputStream(),
        targetLocation,
        StandardCopyOption.REPLACE_EXISTING
      );

      return targetLocation.toFile();
    } catch (IOException ex) {
      System.out.println(
        "Could not store file " + fileName + ". Please try again!"
          + ex
      );
    }
    return null;
  }

  public String moveFileTo(String path, String folderPrefix) throws IOException {
    File old = new File(path);
    File next = new File(folderPrefix + "/" + old.getName());
    Files.move(old.toPath(), next.toPath(), StandardCopyOption.ATOMIC_MOVE);
    return next.getPath();
  }

  public String renameFiel(String path, String newName) {
    File old = new File(path);
    File next = new File(old.getParent() + "/" + newName);
    old.renameTo(next);
    lastRenameFileName = newName;
    return next.getPath();
  }

  public String getLastRenameFileName() {
    return lastRenameFileName;
  }

  public void resetLastRenameFileName() {
    lastRenameFileName = "";
  }

  public void removeFile(String path) {
    try {
      Files.delete(Paths.get(path));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public String getFolderPrefixByFileType(FileType type) {
    switch (type) {
      case IMAGE:
        return FileCommens.IMAGE_URL_PREFIX;
      case AUDIO:
        return FileCommens.AUDIO_URL_PREFIX;
      case VIDEO:
        return FileCommens.VIDEO_URL_PREFIX;
      case PDF:
        return FileCommens.PDF_URL_PREFIX;
      case TEXT:
        return FileCommens.TEXT_URL_PREFIX;
      default:
        return FileCommens.OTHER_URL_PREFIX;
    }
  }

  public FileType getTypeByFileEnding(String ending) {
    if (isOfType(FileCommens.IMAGE_TYPES, ending)) {
      return FileType.IMAGE;
    }
    if (isOfType(FileCommens.AUDIO_TYPES, ending)) {
      return FileType.AUDIO;
    }
    if (isOfType(FileCommens.VIDEO_TYPES, ending)) {
      return FileType.VIDEO;
    }
    if (".pdf".equals(ending)) {
      return FileType.PDF;
    }
    if (isOfType(FileCommens.TEXT_TYPES, ending)) {
      return FileType.TEXT;
    }
    return FileType.OTHER;
  }

  public boolean doseExsit(TmpFileDisplayDTO file) {
    File ioFile = new File(file.getRawPath());
    return ioFile.exists();
  }

  private boolean isOfType(String[] types, String ending) {
    for (String typeEnding : types) {
      if (typeEnding.equals(ending)) {
        return true;
      }
    }
    return false;
  }
}