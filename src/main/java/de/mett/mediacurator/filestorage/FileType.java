package de.mett.mediacurator.filestorage;

public enum FileType {
	IMAGE, TEXT, VIDEO, PDF, AUDIO, LINK, OTHER
}
