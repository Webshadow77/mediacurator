package de.mett.mediacurator.filestorage;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.mett.mediacurator.tmp.TmpFileService;

@Service
public class FileIoWatcher extends Thread {

	@Autowired
	FileIoStorageService ioStorageService;
	@Autowired
	TmpFileService tmpFileService;

	@Override
	public void run() {

		WatchService watchService = null;
		WatchKey key = null;
		try {
			watchService = FileSystems.getDefault().newWatchService();
			ioStorageService.incommingFileLocation.toPath().register(
					watchService,
					StandardWatchEventKinds.ENTRY_CREATE
					);
			while ((key = watchService.take()) != null) {
				for (WatchEvent<?> event : key.pollEvents()) {
					if (event.context().toString()
							.equals(ioStorageService.getLastRenameFileName())) {
						ioStorageService.resetLastRenameFileName();
					} else {
						tmpFileService.updateTmpFileFolderRegistry(
								event.context().toString()
								);
					}
				}
				key.reset();
			}
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}

	}
}
