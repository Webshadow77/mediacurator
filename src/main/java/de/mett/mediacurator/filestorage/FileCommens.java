package de.mett.mediacurator.filestorage;

public class FileCommens {

  public static final String FOLDER_BATH_URL = "tag-db-storage";
  
	public static final String FOLDER_BATH_PATH = "/tag-db-storage";

	public static final String INCOMMING_FILES_URL_PREFIX = FOLDER_BATH_PATH + "/incomming";

	public static final String IMAGE_URL_PREFIX = FOLDER_BATH_PATH + "/images";

	public static final String TEXT_URL_PREFIX = FOLDER_BATH_PATH + "/text";

	public static final String VIDEO_URL_PREFIX = FOLDER_BATH_PATH + "/video";

	public static final String PDF_URL_PREFIX = FOLDER_BATH_PATH + "/pdf";

	public static final String AUDIO_URL_PREFIX = FOLDER_BATH_PATH + "/audio";

	public static final String LINKS_URL_PREFIX = FOLDER_BATH_PATH + "/links";

	public static final String OTHER_URL_PREFIX = FOLDER_BATH_PATH + "/other";

	public static String[] IMAGE_TYPES = { ".ami", ".apx", ".avif", ".bmp", ".bpg", ".brk", ".bw", ".cal", ".cals",
			".cbm", ".cbr", ".cbz", ".cpt", ".cur", ".dds", ".dng", ".exr", ".fif", ".fpx", ".fxo", ".fxs", ".gbr",
			".gif", ".giff", ".heic", ".heif", ".ico", ".iff", ".ilbm", ".lbm", ".iff", ".img", ".jbig2", ".jb2",
			".jp2", ".jpc", ".j2c", ".j2k", ".jpx", ".jpg", ".jpeg", ".jpe", ".jfif", ".jng", ".jxr", ".wdp", ".hdp",
			".kdc", ".koa", ".lbm", ".lwf", ".lwi", ".mac", ".miff", ".msk", ".msp", ".ncr", ".ngg", ".nlm", ".nmp",
			".nol", ".oaz", ".oil", ".pat", ".pbm", ".pcd", ".pct", ".pcx", ".pdb", ".pdd", ".pgf", ".pgm", ".pic",
			".pld", ".png", ".pnm", ".ppm", ".psd", ".pspimage", ".psp", ".qti", ".qtif", ".ras", ".raw", ".rgb",
			".rgba", ".sgi", ".rle", ".tga", ".bpx", ".icb", ".pix ", ".tif", ".tiff", ".webp", ".xbm", ".xcf", ".xpm",
			".ai", ".cdr", ".cgm", ".cmx", ".des", ".design", ".dgn", ".dvg", ".dwg", ".dwf", ".dxf", ".emf", ".eps",
			".fhX", ".fig", ".gbr", ".ger", ".gem", ".geo", ".mba", ".odg", ".pgf", ".tikz", ".plt", ".hpg", ".hp2",
			".pl2", ".prn", ".ps", ".rvt", ".svg", ".swf", ".sxd", ".tvz", ".wmf", ".xaml", ".xar", ".blend", ".dxf",
			".igs", ".stl", ".stp", ".sat", ".wrl", ".wrz", ".fits", ".fit", ".fts" };

	public static String[] AUDIO_TYPES = { ".3gp", ".aa", ".aac", ".aax", ".act", ".aiff", ".alac", ".amr", ".ape",
			".au", ".awb", ".dct", ".dss", ".dvf", ".flac", ".gsm", ".iklax", ".ivs", ".m4a", ".m4b", ".m4p", ".mmf",
			".mp3", ".mpc", ".msv", ".nmf", ".nsf", ".ogg", ".oga", ".mogg", ".opus", ".ra", ".rm", ".raw", ".rf64",
			".sln", ".tta", ".voc", ".vox", ".wav", ".wma", ".wv", ".8svx", ".cda" };

	public static String[] VIDEO_TYPES = { ".webm", ".mkv ", ".flv ", ".flv ", ".vob ", ".ogv", ".ogg", ".drc", ".gif",
			".gifv ", ".mng", ".avi", ".MTS", ".M2TS", ".TS", ".mov", ".qt", ".wmv", ".yuv", ".rm", ".rmvb", ".asf",
			".amv", ".mp4", ".m4p", ".m4v", ".mpg", ".mp2", ".mpeg", ".mpe", ".mpv", ".mpg", ".mpeg", ".m2v", ".m4v",
			".svi", ".3gp", ".3g2", ".mxf", ".roq", ".nsv", ".flv", ".f4v", ".f4p", ".f4a", ".f4b" };

	public static String[] TEXT_TYPES = { ".txt", ".tex"};

}
