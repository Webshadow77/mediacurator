package de.mett.mediacurator.controller;

import java.security.Principal;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Controller to deliver the base navigation pages of the normal User UI
 *
 * @author sebastian.mett
 *
 */
@Controller
public class IndexController implements ApplicationContextAware {

	private ApplicationContext context;

	/**
	 * Redirecting the index url to /home
	 *
	 * @return
	 */
	@GetMapping(value = { "", "/" })
	@ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
	public RedirectView handleMobileNettorechner() {
		return new RedirectView("/home");
	}

	@GetMapping(value = "/shutdown")
	@ResponseBody
	public void shutdownApplication() {
		((ConfigurableApplicationContext) context).close();
		System.exit(0);
	}

	/**
	 * The home page
	 *
	 * @param model
	 * @param principal
	 * @return
	 */
	@GetMapping(value = "/home")
	public ModelAndView getIndex(Model model, Principal principal) {
		model.addAttribute("username", principal.getName());
		model.addAttribute("pageroot", "home");
		return new ModelAndView("index");

	}

	@Override
	public void setApplicationContext(ApplicationContext ctx)
			throws BeansException {
		this.context = ctx;
	}
}
