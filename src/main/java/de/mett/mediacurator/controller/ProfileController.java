package de.mett.mediacurator.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import de.mett.mediacurator.securety.user.AppUserDto;
import de.mett.mediacurator.securety.user.AppUserService;

@Controller
@RequestMapping("/profile")
public class ProfileController {

	@Autowired
	AppUserService userService;
	/**
	 * The page for the user profile, so the user can see his informations
	 * @param model
	 * @param principal
	 * @return
	 */
	@GetMapping
	public ModelAndView getProfile(Model model, Principal principal) {
		buildBasicUserProfilePage(model, principal);
		return new ModelAndView("profile");
	}
	
	/**
	 * Changes the state of the save search propperty
	 * @param model
	 * @param principal
	 * @return
	 */
	@GetMapping(value = "/swoap-save-search")
	public ModelAndView changeSaveSearchPropperty(Model model, Principal principal) {
		boolean sucsess = userService.swapSaveSearch(principal.getName());
		model.addAttribute("sucsess-swoap",sucsess);
		buildBasicUserProfilePage(model, principal);
		return new ModelAndView("profile");
	}
	
	private void buildBasicUserProfilePage(Model model, Principal principal) {
		AppUserDto user = userService.getUser(principal.getName());
		model.addAttribute("username", user.getUsername());
		model.addAttribute("savesearch", user.isSaveSearchEnabled());
		model.addAttribute("pageroot", "profile");
	}
}
