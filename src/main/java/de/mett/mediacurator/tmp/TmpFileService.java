package de.mett.mediacurator.tmp;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import de.mett.db.public_.tables.records.TmpFileRecord;
import de.mett.mediacurator.config.HtmlTagBuilder;
import de.mett.mediacurator.filestorage.FileCommens;
import de.mett.mediacurator.filestorage.FileIoStorageService;
import de.mett.mediacurator.filestorage.FileType;

@Service
public class TmpFileService {

  @Autowired
  private TmpFileRepository repository;
  @Autowired
  private FileIoStorageService ioStorageService;

  @Transactional
  public void updateTmpFileFolderRegistry() {
    for (File f : ioStorageService.getAllFilesInIncommingFolder()) {
      if (repository.findByName(f.getName()) == null) {
        saveInRegistry(f);
      }
    }
  }

  @Transactional
  public void updateTmpFileFolderRegistry(String fileName) {
    File f = new File(FileCommens.INCOMMING_FILES_URL_PREFIX + "/" + fileName);
    if (repository.findByName(f.getName()) != null) {
      saveInRegistry(f);
    }
  }

  @Transactional
  public void saveInRegistry(File file) {
    TmpFileRecord tmpFile = repository.newRecord();
    String name = file.getName();
    tmpFile.setFileName(name);
    tmpFile.setFilePath(file.getPath());
    tmpFile.setFileEnding(name.substring(name.lastIndexOf('.')));
    tmpFile.setFileType(ioStorageService.getTypeByFileEnding(tmpFile.getFileEnding()).toString());
    repository.save(tmpFile);
  }

  public List<TmpFileDisplayDTO> findFirst10Files() {
    return repository.findAll(PageRequest.of(0, 10));
  }

  public List<TmpFileDisplayDTO> findFirst10FilesRandom(String type) {
    List<TmpFileDisplayDTO> files = repository.findFirst10FilesRandom(type);
    List<TmpFileDisplayDTO> cleantOutput = new ArrayList<>();
    for (TmpFileDisplayDTO file : files) {
      if (ioStorageService.doseExsit(file)) {
        file.setPreviewDisplayString(genDisplayPreviewStrings(file));
        cleantOutput.add(file);
      } else {
        deleteById(file.getId());
      }
    }
    return cleantOutput;
  }

  public TmpFileDisplayDTO findByID(int id) {
    TmpFileDisplayDTO file = repository.findById(id);
    file.setFullSourceDisplayString(genDisplayStrings(file));
    return file;
  }

  public TmpFileRecord findRawByID(int id) {
    return repository.findRawById(id);
  }

  public int countAll() {
    return repository.countAllFiles();
  }

  public List<FileGroup> countGroups() {
    return repository.countGroups();
  }

  @Transactional
  public void deleteById(long id) {
    repository.deleteById(id);
  }

  public String genDisplayPreviewStrings(TmpFileDisplayDTO file) {
    String path = file.getRawPath().replace('\\', '/');
    return HtmlTagBuilder.genDisplayPreviewStrings(path, FileType.valueOf(file.getType()));
  }

  public String genDisplayStrings(TmpFileDisplayDTO file) {
    String path = file.getRawPath().replace('\\', '/');
    return HtmlTagBuilder.genDisplayStrings(path, FileType.valueOf(file.getType()));
  }
}
