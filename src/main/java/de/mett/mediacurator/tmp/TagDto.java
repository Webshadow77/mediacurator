package de.mett.mediacurator.tmp;

public class TagDto {

  private Integer tagId;
  private String tagName;

  public TagDto() {

  }

  public Integer getTagId() {
    return tagId;
  }

  public void setTagId(Integer tagId) {
    this.tagId = tagId;
  }

  public String getTagName() {
    return tagName;
  }

  public void setTagName(String tagName) {
    this.tagName = tagName;
  }

}
