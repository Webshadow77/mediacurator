package de.mett.mediacurator.tmp;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import de.mett.db.public_.tables.TmpFile;
import de.mett.mediacurator.file.FileService;
import de.mett.mediacurator.filestorage.FileIoStorageService;

@Controller
@RequestMapping("tmpfiles")
public class TmpFileController {

  @Autowired
  private TmpFileService tmpFileService;
  @Autowired
  private FileIoStorageService ioStorageService;
  @Autowired
  private FileService fileService;

  @GetMapping
  public ModelAndView getIndex(Model model, @RequestParam(name = "type", required = false) String type) {
    model.addAttribute("numberOfFilesInIncommingFolder", tmpFileService.countAll());
    model.addAttribute("first10TmpFiles", tmpFileService.findFirst10FilesRandom(type));
    model.addAttribute("pageroot", "tmpfiles");
    model.addAttribute("exsistingCounts", tmpFileService.countGroups());
    return new ModelAndView("tmp-files/tmp-index");
  }

  /**
   * Uploding one ore multiple files and link them to {@link TmpFile}
   *
   * @param model
   * @param files
   * @param principal
   * @param type
   * @return
   */
  @RequestMapping(value = "/upload", method = { RequestMethod.POST, RequestMethod.PUT })
  public RedirectView putFiles(
    Model model,
    @RequestParam("file") MultipartFile[] files,
    @RequestParam("type") String type
  ) {
    for (MultipartFile f : files) {
      File saved = ioStorageService.storeFile(f);
      tmpFileService.saveInRegistry(saved);
    }
    return new RedirectView("/tmpfiles");
  }

  @GetMapping("/edit")
  public ModelAndView editTmpFile(Model model, @RequestParam("id") int id) {
    model.addAttribute("tmpFile", tmpFileService.findByID(id));
    model.addAttribute("pageroot", "tmpfiles");
    return new ModelAndView("tmp-files/tmp-edit");
  }

  @PostMapping("/persist")
  public RedirectView persistTmpFile(
    Model model,
    TmpFileEditDTO dto,
    @RequestParam(name = "type", required = false) String type
  ) throws IOException {
    fileService.saveNewFile(dto);
    tmpFileService.deleteById(dto.getId());
    List<TmpFileDisplayDTO> tmpFiles = tmpFileService.findFirst10FilesRandom(type);
    if (tmpFiles != null && !tmpFiles.isEmpty()) {
      return new RedirectView("/tmpfiles/edit?id=" + tmpFiles.get(0).getId());
    }
    return new RedirectView("/tmpfiles");
  }

}
