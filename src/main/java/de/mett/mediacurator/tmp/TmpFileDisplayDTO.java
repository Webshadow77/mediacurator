package de.mett.mediacurator.tmp;

/**
 * 
 * @author sme
 *
 */
public class TmpFileDisplayDTO {

  private long id;
  private String name;
  private String rawPath;
  private String type;
  private String previewDisplayString;
  private String fullSourceDisplayString;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getRawPath() {
    return rawPath;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setRawPath(String rawPath) {
    this.rawPath = rawPath;
  }

  public String getPreviewDisplayString() {
    return previewDisplayString;
  }

  public void setPreviewDisplayString(String previewDisplayString) {
    this.previewDisplayString = previewDisplayString;
  }

  public String getFullSourceDisplayString() {
    return fullSourceDisplayString;
  }

  public void setFullSourceDisplayString(String fullSourceDisplayString) {
    this.fullSourceDisplayString = fullSourceDisplayString;
  }

}
