package de.mett.mediacurator.tmp;

import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.rand;
import static org.jooq.impl.DSL.trueCondition;

import java.util.List;

import org.jooq.DSLContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import de.mett.db.public_.tables.TmpFile;
import de.mett.db.public_.tables.records.TmpFileRecord;

@Repository
public class TmpFileRepository {

  private static final TmpFile tTmpFile = TmpFile.TMP_FILE;

  private final DSLContext jooq;

  public TmpFileRepository(DSLContext jooq) {
    this.jooq = jooq;
  }

  public TmpFileRecord newRecord() {
    return jooq.newRecord(tTmpFile);
  }

  public void save(TmpFileRecord newTmpFile) {
    newTmpFile.store();
  }

  public List<FileGroup> countGroups() {
    return jooq.select(
      tTmpFile.FILE_TYPE.as("name"),
      count()
    )
      .from(tTmpFile)
      .groupBy(tTmpFile.FILE_TYPE)
      .orderBy(count())
      .fetchInto(FileGroup.class);
  }

  public List<TmpFileDisplayDTO> findAll(PageRequest page) {
    return jooq.select(
      tTmpFile.TMP_FILE_ID.as("id"),
      tTmpFile.FILE_NAME.as("name"),
      tTmpFile.FILE_PATH.as("rawPath")
    )
      .from(tTmpFile)
      .offset(page.getPageNumber() * page.getPageSize())
      .limit(page.getPageSize())
      .fetchInto(TmpFileDisplayDTO.class);
  }

  public TmpFileDisplayDTO findById(int id) {
    return jooq.select(
      tTmpFile.TMP_FILE_ID.as("id"),
      tTmpFile.FILE_NAME.as("name"),
      tTmpFile.FILE_PATH.as("rawPath"),
      tTmpFile.FILE_TYPE.as("type")
    )
      .from(tTmpFile)
      .where(tTmpFile.TMP_FILE_ID.eq(id))
      .fetchOneInto(TmpFileDisplayDTO.class);
  }

  public TmpFileRecord findRawById(int id) {
    return jooq.selectFrom(tTmpFile)
      .where(tTmpFile.TMP_FILE_ID.eq(id))
      .fetchOne();
  }

  public TmpFileRecord findByName(String name) {
    return jooq.selectFrom(tTmpFile).where(tTmpFile.FILE_NAME.eq(name)).fetchOne();
  }

  public void deleteById(Long id) {
    jooq.delete(tTmpFile).where(tTmpFile.TMP_FILE_ID.eq(id.intValue())).execute();
  }

  public int countAllFiles() {
    return jooq.fetchCount(tTmpFile);
  }

  public List<TmpFileDisplayDTO> findFirst10FilesRandom(String type) {
    return jooq.select(
      tTmpFile.TMP_FILE_ID.as("id"),
      tTmpFile.FILE_NAME.as("name"),
      tTmpFile.FILE_PATH.as("rawPath"),
      tTmpFile.FILE_TYPE.as("type")
    )
      .from(tTmpFile)
      .where(!StringUtils.isEmpty(type) ? tTmpFile.FILE_TYPE.eq(type) : trueCondition())
      .orderBy(rand())
      .limit(10)
      .fetchInto(TmpFileDisplayDTO.class);
  }
}
