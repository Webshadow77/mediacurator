package de.mett.mediacurator.tmp;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sebastian.mett
 *
 */
public class TmpFileEditDTO {

	private int id;
	private List<String> tags = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	public void addTag(String tagName) {
		tags.add(tagName);
	}
}
