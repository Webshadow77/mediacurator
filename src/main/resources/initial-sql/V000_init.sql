-- Triggerfunktion die zum hochzählen von Version und Updated Spalte wieder verwendet werden kann. Muss nur einmal pro Datenbank exsistieren.
-- In Postgresql muss man um einen Trigger zu erzeugen eine Trigger Function (eine Funktion die einen Trigger zurück liefert) erstellen. Diese liefert bei aufruf eine TriggerFunktion zurück. Damit hat jeder Trigger seine eigene Version dieser Funktion die er dann aufruft. Der Trigger muss neu erzeugt werden, wenn man die Funktion ändern will.
-- Damit die Funktion parallel aufgerufen werden kann wird sie als PARALLEL SAFE definiert. (Dokumentation siehe : https://www.postgresql.org/docs/10/parallel-safety.html) 
CREATE OR REPLACE FUNCTION update_version_and_updated_date()
  RETURNS trigger 
  PARALLEL SAFE 
  LANGUAGE PLPGSQL
  AS
$$
BEGIN
	NEW.updated_at := (now() at time zone 'utc');
	NEW.version := NEW.version + 1;
    RETURN NEW;
END;
$$;

COMMENT ON FUNCTION update_version_and_updated_date IS 'Function to rase version by one and updating created_at with the current time without Zeitzohne.';
