-- ---- Neue Tabelle
-- Neue Sequence zum gesichertem zählen der Einträge
CREATE SEQUENCE SEQ_tag;

create table tag (
	-- ID Spalte mit Sequence als default Wert um auch mittels extern vergebener ID über selber sequence arbeiten zu können.
	tag_id					INT 	  default nextval('SEQ_tag'),	
	
	tag_name				varchar(300)	NOT NULL UNIQUE,
	
	version 						INT 		    			NOT NULL default 1,
	created_at						TIMESTAMP without time zone NOT NULL default (now() at time zone 'utc'),
	updated_at						TIMESTAMP without time zone,
	PRIMARY KEY(tag_id)
);

COMMENT ON TABLE  tag IS 'This is the table tag. It is used for XYZ';

COMMENT ON COLUMN tag.tag_id 			IS 'The auto generated id for the tag table. It is the PRIMARY KEY.';
COMMENT ON COLUMN tag.version 					IS 'Version of this entry';
COMMENT ON COLUMN tag.created_at 				IS 'Time in UTC when this entry was created.';
COMMENT ON COLUMN tag.updated_at 				IS 'Time in UTC when this entry updated.';

-- UNIQUE Indexes
CREATE UNIQUE INDEX UI1_tag ON tag (tag_name);

-- Trigger der zum hochzählen von Version und updated Wert
CREATE TRIGGER tbu_tag_was_update_trigger
  BEFORE UPDATE
  ON tag 
  FOR EACH ROW
  EXECUTE PROCEDURE update_version_and_updated_date();

COMMENT ON TRIGGER tbu_tag_was_update_trigger ON tag IS 'Trigger before update (TBU) to call the update version and updated_at function for Table tag.';

