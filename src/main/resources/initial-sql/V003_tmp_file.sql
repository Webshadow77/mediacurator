-- ---- Neue Tabelle
-- Neue Sequence zum gesichertem zählen der Einträge
CREATE SEQUENCE SEQ_tmp_file;

create table tmp_file (
	-- ID Spalte mit Sequence als default Wert um auch mittels extern vergebener ID über selber sequence arbeiten zu können.
	tmp_file_id					INT 	  default nextval('SEQ_tmp_file'),	
	
	file_ending 		    varchar(300) 				NOT NULL,
	file_name 			    varchar(300) 				NOT NULL,
	file_path 			    varchar(300) 				NOT NULL,
	file_type 			    varchar(300) 				NOT NULL,
	
	version 						INT 		    			NOT NULL default 1,
	created_at						TIMESTAMP without time zone NOT NULL default (now() at time zone 'utc'),
	updated_at						TIMESTAMP without time zone,
	PRIMARY KEY(tmp_file_id)
);

COMMENT ON TABLE  tmp_file IS 'This is the table tmp_file. It is used for XYZ';

COMMENT ON COLUMN tmp_file.tmp_file_id 			IS 'The auto generated id for the tmp_file table. It is the PRIMARY KEY.';
COMMENT ON COLUMN tmp_file.version 					IS 'Version of this entry';
COMMENT ON COLUMN tmp_file.created_at 				IS 'Time in UTC when this entry was created.';
COMMENT ON COLUMN tmp_file.updated_at 				IS 'Time in UTC when this entry updated.';

-- Indexes
CREATE INDEX IX1_tmp_file ON tmp_file (file_name);
CREATE INDEX IX2_tmp_file ON tmp_file (file_path);
CREATE INDEX IX3_tmp_file ON tmp_file (file_type);

-- Trigger der zum hochzählen von Version und updated Wert
CREATE TRIGGER tbu_tmp_file_was_update_trigger
  BEFORE UPDATE
  ON tmp_file 
  FOR EACH ROW
  EXECUTE PROCEDURE update_version_and_updated_date();

COMMENT ON TRIGGER tbu_tmp_file_was_update_trigger ON tmp_file IS 'Trigger before update (TBU) to call the update version and updated_at function for Table tmp_file.';

