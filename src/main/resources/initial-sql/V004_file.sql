-- ---- Neue Tabelle
-- Neue Sequence zum gesichertem zählen der Einträge
CREATE SEQUENCE SEQ_file;

create table file (
	-- ID Spalte mit Sequence als default Wert um auch mittels extern vergebener ID über selber sequence arbeiten zu können.
	file_id					INT 	  default nextval('SEQ_file'),

	file_ending 			    varchar(300) 				NOT NULL,
	file_path 				    varchar(300) 				NOT NULL,
	file_type	 			    varchar(300) 				NOT NULL,
	
	version 						INT 		    			NOT NULL default 1,
	created_at						TIMESTAMP without time zone NOT NULL default (now() at time zone 'utc'),
	updated_at						TIMESTAMP without time zone,
	PRIMARY KEY(file_id)
);

COMMENT ON TABLE  file IS 'This is the table file. It is used for XYZ';

COMMENT ON COLUMN file.file_id 			IS 'The auto generated id for the file table. It is the PRIMARY KEY.';
COMMENT ON COLUMN file.version 					IS 'Version of this entry';
COMMENT ON COLUMN file.created_at 				IS 'Time in UTC when this entry was created.';
COMMENT ON COLUMN file.updated_at 				IS 'Time in UTC when this entry updated.';

-- Indexes
CREATE INDEX IX1_file ON file (created_at);
CREATE INDEX IX2_file ON file (updated_at);
CREATE INDEX IX3_file ON file (file_type);

-- UNIQUE Indexes
CREATE UNIQUE INDEX UI1_file ON file (file_path);

-- Trigger der zum hochzählen von Version und updated Wert
CREATE TRIGGER tbu_file_was_update_trigger
  BEFORE UPDATE
  ON file 
  FOR EACH ROW
  EXECUTE PROCEDURE update_version_and_updated_date();

COMMENT ON TRIGGER tbu_file_was_update_trigger ON file IS 'Trigger before update (TBU) to call the update version and updated_at function for Table file.';

