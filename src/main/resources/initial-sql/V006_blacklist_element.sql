-- ---- Neue Tabelle
-- Neue Sequence zum gesichertem zählen der Einträge
CREATE SEQUENCE SEQ_blacklist_element;

create table blacklist_element (
	-- ID Spalte mit Sequence als default Wert um auch mittels extern vergebener ID über selber sequence arbeiten zu können.
	blacklist_element_id					INT 	  default nextval('SEQ_blacklist_element'),	
	
	tag_id 			    INT 				NOT NULL UNIQUE,
	
	version 						INT 		    			NOT NULL default 1,
	created_at						TIMESTAMP without time zone NOT NULL default (now() at time zone 'utc'),
	updated_at						TIMESTAMP without time zone,
	PRIMARY KEY(blacklist_element_id),
	
	CONSTRAINT fk1_blacklist_element
       FOREIGN KEY(tag_id) 
	   REFERENCES tag(tag_id)
);

COMMENT ON TABLE  blacklist_element IS 'This is the table blacklist_element. It is used for XYZ';

COMMENT ON COLUMN blacklist_element.blacklist_element_id 			IS 'The auto generated id for the blacklist_element table. It is the PRIMARY KEY.';
COMMENT ON COLUMN blacklist_element.version 					IS 'Version of this entry';
COMMENT ON COLUMN blacklist_element.created_at 				IS 'Time in UTC when this entry was created.';
COMMENT ON COLUMN blacklist_element.updated_at 				IS 'Time in UTC when this entry updated.';

-- UNIQUE Indexes
CREATE UNIQUE INDEX UI1_blacklist_element ON blacklist_element (tag_id);

-- Trigger der zum hochzählen von Version und updated Wert
CREATE TRIGGER tbu_blacklist_element_was_update_trigger
  BEFORE UPDATE
  ON blacklist_element 
  FOR EACH ROW
  EXECUTE PROCEDURE update_version_and_updated_date();

COMMENT ON TRIGGER tbu_blacklist_element_was_update_trigger ON blacklist_element IS 'Trigger before update (TBU) to call the update version and updated_at function for Table blacklist_element.';

