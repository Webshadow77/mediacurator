-- ---- Neue Tabelle
-- Neue Sequence zum gesichertem zählen der Einträge
CREATE SEQUENCE SEQ_app_user;

create table app_user (
	-- ID Spalte mit Sequence als default Wert um auch mittels extern vergebener ID über selber sequence arbeiten zu können.
	app_user_id					INT 	  default nextval('SEQ_app_user'),
	
	username					varchar(200)	NOT NULL UNIQUE,
	password					varchar(300)	NOT NULL,
	locked						boolean			NOT NULL	default false,
	enabled						boolean			NOT NULL	default true,
	save_search_enabled			boolean			NOT NULL	default false,
	
	version 						INT 		    			NOT NULL default 1,
	created_at						TIMESTAMP without time zone NOT NULL default (now() at time zone 'utc'),
	updated_at						TIMESTAMP without time zone,
	PRIMARY KEY(app_user_id)
);

COMMENT ON TABLE  app_user IS 'This is the table app_user. It is used for XYZ';

COMMENT ON COLUMN app_user.app_user_id 			IS 'The auto generated id for the app_user table. It is the PRIMARY KEY.';
COMMENT ON COLUMN app_user.version 					IS 'Version of this entry';
COMMENT ON COLUMN app_user.created_at 				IS 'Time in UTC when this entry was created.';
COMMENT ON COLUMN app_user.updated_at 				IS 'Time in UTC when this entry updated.';

-- UNIQUE Indexes
CREATE UNIQUE INDEX UI1_app_user ON app_user (username);

-- Trigger der zum hochzählen von Version und updated Wert
CREATE TRIGGER tbu_app_user_was_update_trigger
  BEFORE UPDATE
  ON app_user 
  FOR EACH ROW
  EXECUTE PROCEDURE update_version_and_updated_date();

COMMENT ON TRIGGER tbu_app_user_was_update_trigger ON app_user IS 'Trigger before update (TBU) to call the update version and updated_at function for Table app_user.';

