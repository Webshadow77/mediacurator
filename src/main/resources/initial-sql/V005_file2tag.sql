-- ---- Neue Tabelle
-- Neue Sequence zum gesichertem zählen der Einträge
CREATE SEQUENCE SEQ_file2tag;

create table file2tag (
	-- ID Spalte mit Sequence als default Wert um auch mittels extern vergebener ID über selber sequence arbeiten zu können.
	file2tag_id					INT 	  default nextval('SEQ_file2tag'),	
	
	file_id 			    	INT 				NOT NULL,
	tag_id 			   		 	INT 				NOT NULL,
	
	version 						INT 		    			NOT NULL default 1,
	created_at						TIMESTAMP without time zone NOT NULL default (now() at time zone 'utc'),
	updated_at						TIMESTAMP without time zone,
	
	PRIMARY KEY(file2tag_id),
	
	CONSTRAINT fk1_file2tag
       FOREIGN KEY(file_id) 
	   REFERENCES file(file_id),
	CONSTRAINT fk2_file2tag
       FOREIGN KEY(tag_id) 
	   REFERENCES tag(tag_id)
);

COMMENT ON TABLE  file2tag IS 'This is the table file2tag. It is used for XYZ';

COMMENT ON COLUMN file2tag.file2tag_id 			IS 'The auto generated id for the file2tag table. It is the PRIMARY KEY.';
COMMENT ON COLUMN file2tag.version 					IS 'Version of this entry';
COMMENT ON COLUMN file2tag.created_at 				IS 'Time in UTC when this entry was created.';
COMMENT ON COLUMN file2tag.updated_at 				IS 'Time in UTC when this entry updated.';

-- Indexes
CREATE INDEX IX1_file2tag ON file2tag (file_id);
CREATE INDEX IX2_file2tag ON file2tag (tag_id);

-- UNIQUE Indexes
CREATE UNIQUE INDEX UI1_file2tag ON file2tag (file_id, tag_id);

-- Trigger der zum hochzählen von Version und updated Wert
CREATE TRIGGER tbu_file2tag_was_update_trigger
  BEFORE UPDATE
  ON file2tag 
  FOR EACH ROW
  EXECUTE PROCEDURE update_version_and_updated_date();

COMMENT ON TRIGGER tbu_file2tag_was_update_trigger ON file2tag IS 'Trigger before update (TBU) to call the update version and updated_at function for Table file2tag.';

