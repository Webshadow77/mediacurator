import { Http } from "../http/Http.js";
import { Header } from "../http/Header.js";

class FileSingleView {

	deleteButton: HTMLElement;
	id: string;

	constructor() {
		this.deleteButton = document.getElementById('deleteButton');
		this.id = document.getElementById('fileUUID').innerText;
		this.bindBehaviour();
	}

	bindBehaviour() {
		let self = this;
		this.deleteButton.addEventListener('click', function(event) {
			event.preventDefault();
			self.deleteFile();
		});
	}

	deleteFile() {
		let self = this;
		let http = new Http();
		http.post('/files/delete', {
			headers: new Header(),
			params: ('?id='+ self.id)
		}).then(() => {
			window.location.href = '/files/explore';
		});
	}
}

let fileSingleView: FileSingleView = new FileSingleView();