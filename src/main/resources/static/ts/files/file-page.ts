import { Http } from "../http/Http.js";
import { Header } from "../http/Header.js";
import { SuggestFunction } from "../suggestFunction.js";
import { Pagination } from "../pagination.js";

class FileAndSearchPage {

	tagInputFiled: HTMLInputElement;
	tagCollection: HTMLElement;
	initialImgCollection: HTMLElement;
	paginationContainer: HTMLElement;
	pagination: Pagination;
	elemRequestNr: number;

	constructor() {
		this.tagInputFiled = document.getElementById('tag-input-filed') as HTMLInputElement;
		this.tagCollection = document.getElementById('tags-to-search');
		this.initialImgCollection = document.getElementById('initial-images');
		this.paginationContainer = document.getElementById('pagination-row');
		this.elemRequestNr = 20;
		this.pagination = new Pagination(this.paginationContainer, this.elemRequestNr, this.loadNewFilesByTagsSimple.bind(this));
	}

	addTagToCollection(tagname: string, tagID: string) {
		let tag = document.createElement('div');
		tag.id = tagID;
		tag.classList.add('btn', 'btn-success', 'tagstyle');
		let span = document.createElement('span');
		span.classList.add('glyphicon', 'glyphicon-asterisk');
		tag.appendChild(span);
		let text = document.createTextNode(tagname);
		tag.appendChild(text);
		this.tagCollection.appendChild(tag);
	}

	getAllTags() {
		let output: string[] = [];
		let children = this.tagCollection.children;
		for (let i = 0; i < children.length; i++) {
			output[i] = children[i].lastChild.textContent;
		}
		return output;
	}

	deleteTag(tagID: string) {
		let tag: HTMLElement = document.getElementById(tagID);
		tag.parentElement.removeChild(tag);
		this.loadNewFilesByTags(this.getAllTags(), 0, this.elemRequestNr+1, page.pagination.updateNavigationButtons);
	}

	loadNewFilesByTagsSimple(pageNr: number, pageSize: number, callback: Function) {
		this.loadNewFilesByTags(this.getAllTags(), pageNr, pageSize, callback);
	}

	loadNewFilesByTags(tags: string[], pageNr: number, pageSize: number, callback: Function) {
		let self = this;
		let http = new Http();
		http.get('/files/searchrequest', {
			headers: new Header(),
			params: ('?tags=' + tags + "&pageNr=" + pageNr + "&pageSize=" + pageSize)
		}).then((res) => {
			let data: DisplayFile[] = JSON.parse((res as XMLHttpRequest).responseText);
			self.updateFileList(data);
			callback(data.length);
		});
	}

	updateFileList(files: DisplayFile[]) {
		this.initialImgCollection.innerHTML = '';

		for (let i = 0; i < files.length && i < this.pagination.elementsToShow - 1; i++) {
			let a: HTMLAnchorElement = document.createElement('a');
			a.classList.add('col-lg-3');
			a.href = '/files/singel?uuid=' + files[i].id;
			a.innerHTML = files[i].previewDisplayString;
			this.initialImgCollection.appendChild(a);
		}
	}
}

class DisplayFile {
	id: string;
	previewDisplayString: string;
	fullSourceDisplayString: string;
	tags: string[];
}

let page: FileAndSearchPage = new FileAndSearchPage();
let addToTag: Function = function(value: string) {
	let tagID = 'button-' + value;
	page.addTagToCollection(value, tagID);
	document.getElementById(tagID).addEventListener('click', function(e) {
		page.deleteTag(tagID);
	});
	page.loadNewFilesByTagsSimple(0, page.elemRequestNr+1, page.pagination.updateNavigationButtons.bind(page.pagination));
}

let suggesftFunktion: SuggestFunction = new SuggestFunction(page.tagInputFiled, '/tags/suggestion', true, addToTag);