import { Http } from "../http/Http.js";
import { Header } from "../http/Header.js";
import { Pagination } from "../pagination.js";

class FileByTagPage {

	paginationContainer: HTMLElement;
	initialImgCollection: HTMLElement;
	pagination: Pagination;
	elemRequestNr: number;
	tagname: string;

	constructor() {
		this.elemRequestNr = 20;
		this.tagname = document.getElementById('tagname').innerText;
		this.paginationContainer = document.getElementById('pagination-row');
		this.initialImgCollection = document.getElementById('initial-images');
		this.pagination = new Pagination(this.paginationContainer, this.elemRequestNr, this.loadNewFilesByTags.bind(this));
		let n: number = Number(document.getElementById('initialSize').innerText);
		if (Number(document.getElementById('initialSize').innerText) > 20) {
			this.pagination.activateButton(this.pagination.ahaedButton);
		}
	}
	
	loadNewFilesByTags(pageNr: number, pageSize: number, callback: Function) {
		let self = this;
		let http = new Http();
		http.get('/files/searchrequest', {
			headers: new Header(),
			params: ('?tags=' + this.tagname + "&pageNr=" + pageNr + "&pageSize=" + pageSize)
		}).then((res) => {
			let data: DisplayFile[] = JSON.parse((res as XMLHttpRequest).responseText);
			self.updateFileList(data);
			callback(data.length);
		});
	}
	
	updateFileList(files: DisplayFile[]){
		this.initialImgCollection.innerHTML = '';

		for (let i = 0; i < files.length && i < this.pagination.elementsToShow - 1; i++) {
			let a: HTMLAnchorElement = document.createElement('a');
			a.classList.add('col-lg-3');
			a.href = '/files/singel?uuid=' + files[i].id;
			a.innerHTML = files[i].previewDisplayString;
			this.initialImgCollection.appendChild(a);
		}
	}
}

class DisplayFile {
	id: string;
	previewDisplayString: string;
	fullSourceDisplayString: string;
	tags: string[];
}

let fileByTagPage: FileByTagPage = new FileByTagPage();