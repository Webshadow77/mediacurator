import { Http } from "../http/Http.js";
import { Header } from "../http/Header.js";
import { Pagination } from "../pagination.js";

class TagPage {

	lastResultSize: number;
	paginationContainer: HTMLElement;
	tag_countainer: HTMLElement;
	newTagInputField: HTMLInputElement;
	pagination: Pagination;

	constructor() {
		this.lastResultSize = 0;
		this.paginationContainer = document.getElementById('pagination-row');
		this.tag_countainer = document.getElementById('taglist');
		this.newTagInputField = document.getElementById('newTagInput') as HTMLInputElement;
		this.pagination = new Pagination(this.paginationContainer, 50, this.update_taglist.bind(this));
		this.init_taglist();
		this.bindBehaviour();
	}

	init_taglist() {
		this.update_taglist(this.pagination.pageNr, this.pagination.pageSize, this.pagination.updateNavigationButtons.bind(this.pagination));
	}

	update_taglist(pagenr: number, pagesize: number, callback: Function) {
		let self = this;
		self.tag_countainer.innerHTML = '';

		let http = new Http();
		http.get('/tags/list', {
			headers: new Header(),
			params: ('?page=' + pagenr + '&respp=' + pagesize)
		}).then((res) => {
			let data = JSON.parse((res as XMLHttpRequest).responseText);
			self.lastResultSize = data.length;
			let newContent = '';
			newContent += '<div class="col-lg-4">';
			let first_car_header = data[0].charAt(0)
				.toUpperCase();
			newContent += '<div style="background-color: #eee;"><h3>#'
				+ first_car_header + '</h3>';
			let overAllDepth = 2;
			let inDepth = 0;
			let colNumber = 0;
			let rowSwap = false;
			for (let i = 0, l = data.length; i < l; i++) {
				if (data[i].charAt(0).toUpperCase() != first_car_header) {
					first_car_header = data[i].charAt(0)
						.toUpperCase();
					if (!rowSwap) {
						newContent += '</div>';
					} else {
						rowSwap = false;
					}
					newContent += '<div style="background-color: #eee;"><h3>#'
						+ first_car_header + '</h3>';
					overAllDepth += 2;
					inDepth = 0;
				}
				let newTag = '<a href="/files/by-tag?tag='
					+ data[i]
					+ '"><div class="btn btn-success tagstyle" style="display: block;">'
					+ data[i] + '</div></a>';
				newContent += newTag;
				inDepth++;
				if (inDepth > 1) {
					overAllDepth++;
					inDepth = 0;
				}
				if (overAllDepth > 15 && colNumber < 2) {
					newContent += '</div>';
					newContent += '</div>';
					newContent += '<div class="col-lg-4">';
					overAllDepth = 0;
					colNumber++;
					rowSwap = true;
				}
			}
			newContent += '</div>';
			newContent += '</div>';
			self.tag_countainer.innerHTML = newContent + '<div class="col-lg-4"></div>' + '<div class="col-lg-4"></div>';
			callback(data.length);
		});
	}

	addTag() {
		let self = this;

		let http = new Http();
		http.post('/tags/add', {
			headers: new Header(),
			params: ('?name=' + self.newTagInputField.value.toLowerCase())
		}).then(() => {
			self.newTagInputField.value = '';
			self.update_taglist(0, 40, self.pagination.updateNavigationButtons.bind(self));
		});
	}

	bindBehaviour() {
		let self = this;

		document.getElementById("addTagForm").addEventListener("submit",
			function(event) {
				event.preventDefault();
				self.addTag();
			});
	}
}

let tagPage = new TagPage();