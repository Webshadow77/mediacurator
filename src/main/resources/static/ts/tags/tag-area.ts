import { SuggestFunction } from "../suggestFunction.js";
import { convertTagsStringToList } from "./tag-in-ex-port.js";

class TagArea {

	tagCounter: number;
	inputField: HTMLInputElement;
	addBUtton: HTMLElement;
	tagarea: HTMLElement;

	constructor() {
		this.tagCounter = 0;
		this.inputField;
		this.addBUtton;
		this.tagarea;
		this.initHTML();
		this.loadExsistingTags();
		this.bindBehaviour();
		window.scrollTo(0, document.body.scrollHeight);
	}

	initHTML() {
		let rootElem: HTMLElement = document.getElementById('tagarea');
		let inputName = 'tag-inputfield';
		let addButtonName = 'addTagButton';

		let area = document.createElement('div');
		area.classList.add('row');

		let form = document.createElement('form');
		form.id = 'testForm';
		area.appendChild(form);

		let div1 = document.createElement('div');
		div1.classList.add('col-lg-1');
		div1.style.padding = '0 0 0 0';
		form.appendChild(div1);

		let div2 = document.createElement('div');
		div2.classList.add('btn', 'btn-success');
		div2.id = addButtonName;
		div2.style.width = '100%';
		div2.appendChild(document.createTextNode('Add '));
		let addSpan = document.createElement('span');
		addSpan.classList.add('glyphicon', 'glyphicon-plus');
		div2.appendChild(addSpan);
		div1.appendChild(div2);

		let div3 = document.createElement('div');
		div3.classList.add('col-lg-11', 'autocomplete');
		let tagInput: HTMLInputElement = document.createElement('input') as HTMLInputElement;
		tagInput.type = 'text';
		tagInput.autocomplete = 'off';
		tagInput.classList.add('form-control');
		tagInput.id = inputName;
		tagInput.placeholder = 'tags';
		tagInput.autofocus = true;
		div3.appendChild(tagInput);
		form.appendChild(div3);

		rootElem.appendChild(area);

		let tagRowName = 'tagrow';
		let tagRowElem = document.createElement('div');
		tagRowElem.id = tagRowName;
		tagRowElem.classList.add('row');
		rootElem.appendChild(tagRowElem);
		this.inputField = document.getElementById(inputName) as HTMLInputElement;
		this.addBUtton = document.getElementById(addButtonName);
		this.tagarea = document.getElementById(tagRowName);
	}

	addTagToTagCollection() {

		let tagname = this.inputField.value;
		let tags: string[] = [];
		if (tagname.indexOf('$$') == 0) {
			tags = convertTagsStringToList(tagname);
		} else {
			tags[0] = tagname;
		}
		for (let i: number = 0; i < tags.length; ++i) {
			let tagname = tags[i].toLowerCase();
			tagArea.addTagAndBehavior(tagname);
			tagArea.tagCounter++;
			tagArea.inputField.value = '';
		}
	}
	addTagAndBehavior(tagname: string) {
		this.tagarea.appendChild(this.buildTag(tagname));
		this.bindDeleteFuntion(this.buildDynamicTagId(tagname));
	}
	buildTag(tagname: string) {
		let tag = document.createElement('div');
		tag.id = this.buildDynamicTagId(tagname);
		tag.classList.add('btn', 'btn-success', 'tagstyle');

		let anotherDiv = document.createElement('div');
		tag.appendChild(anotherDiv);

		let glyfSpan = document.createElement('span');
		glyfSpan.classList.add('glyphicon', 'glyphicon-asterisk');
		anotherDiv.appendChild(glyfSpan);

		let text = document.createTextNode(tagname);
		anotherDiv.appendChild(text);

		let inputElement: HTMLInputElement = document.createElement('input') as HTMLInputElement;
		inputElement.id = 'tag' + this.tagCounter;
		inputElement.value = tagname;
		inputElement.hidden = true;
		inputElement.name = 'tags[' + this.tagCounter + ']';
		inputElement.type = 'text';
		tag.appendChild(inputElement);

		return tag;
	}
	buildDynamicTagId(tagname: string) {
		return tagname + this.tagCounter;
	}

	loadExsistingTags() {
		let self = this;
		let exsistingTagsArea = document.getElementById('exsistingTags');
		if (exsistingTagsArea) {
			exsistingTagsArea.childNodes.forEach(function(element: HTMLElement) {
				if (element.localName === 'span') {
					let tagname = element.textContent;
					self.addTagAndBehavior(tagname);
					self.tagCounter++;
				}
			});
			exsistingTagsArea.remove();
		}
	}

	bindDeleteFuntion(tagID: string) {
		let self = this;
		document.getElementById(tagID).addEventListener('click', function(event) {
			self.deleteTag(this);
		});
	}
	deleteTag(element: HTMLElement) {
		element.remove();
	}

	bindBehaviour() {
		let self = this;

		document.getElementById('testForm').addEventListener('submit', function(event) {
			event.preventDefault();
			self.addTagToTagCollection();
		})

		this.addBUtton.addEventListener('click', function(event) {
			self.addTagToTagCollection();
		});
	}
}

let tagArea = new TagArea();
let addTag = function(value: string) {
	let tags: string[] = [];
	if (value.indexOf('$$') == 0) {
		tags = convertTagsStringToList(value);
	} else {
		tags[0] = value;
	}
	for (let i: number = 0; i < tags.length; ++i) {
		let tagname = tags[i].toLowerCase();
		tagArea.addTagAndBehavior(tagname);
		tagArea.tagCounter++;
		tagArea.inputField.value = '';
	}
}

let suggestFunction = new SuggestFunction(tagArea.inputField, '/tags/suggestion', true, addTag);
tagArea.inputField.addEventListener('keydown', function(event) {
	if (event.keyCode == 13 && suggestFunction.currentFocus < 0) {
		tagArea.addTagToTagCollection();
		suggestFunction.closeAllLists(null);
	}
});