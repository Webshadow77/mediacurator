import { Http } from "../http/Http.js";
import { Header } from "../http/Header.js";

class TagEditPage {

	originalname: HTMLElement;
	tagname: HTMLInputElement;
	blacklist: HTMLInputElement;

	constructor() {
		this.originalname = document.getElementById('originalname');
		this.tagname = document.getElementById('tagname') as HTMLInputElement;
		this.blacklist = document.getElementById('blacklist') as HTMLInputElement;
		this.bindBehaviour();
	}

	bindBehaviour() {
		let self = this;
		document.getElementById('submitForm').addEventListener('submit', function(event) {
			event.preventDefault();
			self.editTag();
		});
	}

	editTag() {
		let self = this;
		
		let http = new Http();
		http.post('/tags/edit', {
					headers: new Header(),
					params: ('?originalname='+ self.originalname.textContent
							 + '&updatedname=' + self.tagname.value.toLowerCase()
							 + '&blacklist=' + self.blacklist.checked
						)
				}).then(() => {
					window.location.href = '/tags/optins';
				});
	}
}

let tagEditPage = new TagEditPage();