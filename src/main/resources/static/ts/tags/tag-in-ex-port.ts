import { copyToClipboard } from "../copyToClipboardFunction.js";

class TagExportFunktion {

	exportButton: HTMLElement;

	constructor() {
		this.exportButton = document.getElementById('tagexport');
		this.exportButton.addEventListener('click', function(e: Event) {
			
			let exsistingTags = document.getElementsByClassName('tagData');
			if (exsistingTags) {
				let exportString = '$';
				for(let i:number = 0; i < exsistingTags.length; i++){
					exportString += '$' + (exsistingTags.item(i) as HTMLElement).dataset.tagname;
				}
				copyToClipboard(exportString);
			}
		});
	}
}

export function convertTagsStringToList(exportetTagList: string){
	let tags:string[] = [];
	if(exportetTagList.indexOf('$$') == 0){
		exportetTagList = exportetTagList.substring(2);
		tags = exportetTagList.split('$');
	}
	return tags;
}

let exportFuntion: TagExportFunktion = null;
if (document.getElementById('tagexport')) {
	exportFuntion = new TagExportFunktion();
}
