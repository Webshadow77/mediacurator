import { Http } from "../http/Http.js";
import { Header } from "../http/Header.js";


class TagSearchWidget {

	rootDiv: HTMLElement;
	searchFormName: string;
	inputFieldName: string;
	searchForm: HTMLFormElement;
	inputField: HTMLInputElement;
	tag_countainer: HTMLElement;
	larstSearchText: string;

	constructor() {
		this.rootDiv = document.getElementById('tagSearchWidget');
		this.searchFormName = 'tagSearchForm';
		this.inputFieldName = 'tagNameInput';
		this.buildHTMLContent();
		this.searchForm = document.getElementById(this.searchFormName) as HTMLFormElement;
		this.inputField = document.getElementById(this.inputFieldName) as HTMLInputElement;
		this.tag_countainer = document.getElementById('taglist');

		this.bindBehaviour();
		this.search();
	}

	buildHTMLContent() {
		let self = this;
		this.rootDiv.innerHTML = this.rootDiv.innerHTML 
				+ '<input id="'
				+ self.inputFieldName
				+ '" type="text" class="form-control" placeholder="Tagname" autocomplete="off">';
	}
	search() {
		this.update_taglist(this.inputField.value);
	}

	update_taglist(searchText: string) {
		this.larstSearchText = searchText;
		let self = this;
		self.tag_countainer.innerHTML = '';

		let http = new Http();
		http.get('/tags/suggestion', {
			headers: new Header(),
			params: ('?searchText=' + this.larstSearchText)
		}).then((res) => {
			let data = JSON.parse((res as XMLHttpRequest).responseText);
			
			
			let table: HTMLTableElement = document.createElement('table') as HTMLTableElement;
			table.id = 'tagListTable';
			table.classList.add('table','table-hover');
  			var tbdy: HTMLTableSectionElement = document.createElement('tbody') as HTMLTableSectionElement;
			
			for (let i = 0, l = data.length; i < l; i++) {
				let buttonContent = data[i];
				
				let tRow: HTMLTableRowElement = document.createElement('tr') as HTMLTableRowElement;
				
				let col1: HTMLTableDataCellElement = document.createElement('td') as HTMLTableDataCellElement;
				col1.textContent = buttonContent;				
				tRow.appendChild(col1);
				
				let col2: HTMLTableDataCellElement = document.createElement('td') as HTMLTableDataCellElement;
				let a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
				a.href = '/tags/edit?name=' + encodeURIComponent(buttonContent);
				a.classList.add('btn','btn-primary','glyphicon','glyphicon-pencil');
				col2.appendChild(a);
				tRow.appendChild(col2);
				
				let col3: HTMLTableDataCellElement = document.createElement('td') as HTMLTableDataCellElement;
				let div: HTMLDivElement = document.createElement('div') as HTMLDivElement;
				div.id = 'deleteButtonTag' + buttonContent;
				div.classList.add('glyphicon','glyphicon-trash','btn','btn-danger');
				
				div.addEventListener('click', function() {
					self.deleteTag(buttonContent);
				});
				
				col3.appendChild(div);
				tRow.appendChild(col3);
				
				tbdy.appendChild(tRow);
			}
			table.appendChild(tbdy);
			self.tag_countainer.appendChild(table);
		});
	}
	deleteTag(tagname: string) {
		let self = this;

		let http = new Http();
		http.post('/tags/delite', {
			headers: new Header(),
			params: ('?name=' + tagname)
		}).then(() => {
			self.update_taglist(this.larstSearchText);
		});
	}
	
	bindBehaviour() {
		let self = this;

		self.inputField.addEventListener('keyup', function() {
			event.preventDefault();
			self.search();
		});
	}
}


let tagSearchWidget = new TagSearchWidget();