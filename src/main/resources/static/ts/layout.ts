import { Http } from "./http/Http.js";
import { Header } from "./http/Header.js";

export class Layout {
	
	shutdownButton: HTMLButtonElement;
	
	constructor(){
		this.shutdownButton = document.getElementById('shutdown-btn') as HTMLButtonElement;
		this.shutdownButton.addEventListener('click', function(){
			let http = new Http();
			http.get( '/shutdown' , {
				headers: new Header()
			}).then((response) => {
				console.log('Will shutdown');
			});
		});
	}
}

let layout:Layout = new Layout();