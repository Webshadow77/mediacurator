/**
 * Apple Push-ID
 * https://developer.apple.com/account/ios/identifier/websitePushId
 */
export enum ApplePushId {
    GDE = "web.de.gehalt.pushnotification",
    GV = "web.com.gehaltsvergleich.pushnotification"
}

export enum BreakPoint {
    SMALL = "SMALL",
    MEDIUM = "MEDIUM",
    LARGE = "LARGE",
	large = 1000,
	small = 767,
}

export class Browser {
    type: BrowserType;
}

export enum BrowserType {
    CHROME_FCM = "CHROME_FCM",
    FIREFOX = "FIREFOX",
    SAFARI = "SAFARI",
    INTERNET_EXPLORER = "INTERNET_EXPLORER",
    INTERNET_EXPLORER_EDGE = "INTERNET_EXPLORER_EDGE",
    OPERA = "OPERA",
    UNKNOWN = "UNKNOWN"
}

/**
 * Header Widget Typen
 * https://wiki.personalmarkt.de/pages/viewpage.action?pageId=15007953
 */
export enum HeaderWidgetType {
    SEARCH_WITH_LOCATION_FILTER = "SEARCH_WITH_LOCATION_FILTER",
    SEARCH_WITH_LOCATION = "SEARCH_WITH_LOCATION",
    NOSEARCH = "NOSEARCH",
    HOMESEARCH = "HOMESEARCH"
}

export enum ModalType {
    exit,
    scroll,
    default
}

/**
 * Browser Push System Popup for Permission Tracking
 * Quelle: https://wiki.personalmarkt.de/display/BI/event-pushnotifications
 */
export enum PushnotificationSystemPopupTrackingEventTypes {
    AUTHORIZATION = "AUTHORIZATION",
    DEACTIVATION = "DEACTIVATION", // wird im Backend getrackt
    REJECT_AUTHORIZATION = "REJECT_AUTHORIZATION",
    TEMPORARY_REJECT_AUTHORIZATION = "TEMPORARY_REJECT_AUTHORIZATION"
}

/**
 * PushNotification Confirmation Modal Tracking
 * Quelle: https://wiki.personalmarkt.de/display/BI/event-pushnotifications
 */
export enum PushnotificationModalConfimationTrackingEventTypes {
    SIGNUP = "SIGNUP", // wird im Backend getrackt
    REJECT_SIGNUP = "REJECT_SIGNUP",
    INTENT_SIGNUP = "INTENT_SIGNUP"
}

/**
 * PushNotification Registration Modal Tracking
 * Quelle: https://wiki.personalmarkt.de/display/BI/event-pushnotifications
 */
export enum PushnotificationModalPermissionTrackingEventTypes {
    SHOW_REGISTRATION_POPUP = "SHOW_REGISTRATION_POPUP",
}

/**
 * Browser Modal for Permission
 * granted | default | blocked
 *
 */
export enum PushnotificationPermissionStatus {
    GRANTED = "granted",
    DEFAULT = "default",
    DENIED = "denied"
}

export enum RequestMethod {
    Get = "get",
    Post = "post",
}

export enum SalaryType {
    month = "MONTH",
    year = "YEAR"
}

export enum Status {
    PENDING = "PENDING",
    REJECTED = "REJECTED",
    SUCCEEDED = "SUCCEEDED",
    IDLE = "IDLE"
}

export enum AlertType {
	error = "error",
	success = "success",
	warning = "warning",
	info = "info"
}

export enum LogLevel {
    debug = "DEBUG",
    info = "INFO",
    warning = "WARNING",
    error = "ERROR"
}

export enum TabunderTrigger {
    JOB_AND_LOCATION = "JOB_AND_LOCATION",
    LOCATION = "LOCATION",
    JOB = "JOB",
    PUSH = "PUSH",
    JOBLETTER = "JOBLETTER"
}