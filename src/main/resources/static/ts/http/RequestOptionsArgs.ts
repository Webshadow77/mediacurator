import {Header} from "./Header";

export interface RequestOptionsArgs {
    params?: string | {
        [key: string]: any | any[];
    } | null;
    headers?: Header | null;
    body?: any;
    withCredentials?: boolean | null;
    redirect?: string;
    referrer?: string;
    cache?: string;
    credentials?: string;
    mode?: string;
}
