import {RequestOptionsArgs} from "./RequestOptionsArgs.js";
import {HttpRequestManager} from "./HttpRequestManager.js";
import {RequestMethod} from "./Enum.js";


export class Http {
    /**
     * Performs a request with `get` http method.
     */
    get(url: string, options?: RequestOptionsArgs): Promise<any> {
        return HttpRequestManager.sendAsync(url, RequestMethod.Get, options);
    }

    /**
     * Performs a request with `post` http method.
     */
    post(url: string, options: RequestOptionsArgs): Promise<any> {
        return HttpRequestManager.sendAsync(url, RequestMethod.Post, options);
    }

    /**
     * Sends a beacon request with Content-Type: plain/text
     * If beacon isn't available, will send a HTTP POST request synchronously and block the calling thread until a response or a timeout is reached
     *
     * Take care: This method does NOT work with CSRF-Tokens because custom headers can NOT be added.
     * @param url
     * @param body
     */
    blockingPost(url: string, body: string) {
        if (navigator.sendBeacon) {
            navigator.sendBeacon(url, body);
        } else {
            HttpRequestManager.sendSync(url, RequestMethod.Post, {body: body});
        }
    }
}