export class Header {

    private header: { [key: string]: Head } = {};
    /**
     * add a new http header.
     *
     * @param  header header type.
     * @param  value value of header type.
     */
    public set(header: string, value: string): void {
        if (!this.header[header]) {
            this.header[header] = {
                header: header,
                value: value
            }
        } else {
            throw "Header bereits vorhanden";
        }
    }
    /**
     * get a http header.
     *
     * @param   header type.
     * @return  Header
     */
    public get(header: string): Head | boolean {
        return (this.header[header]) ? this.header[header] : false;
    }
    /**
     * delete a http header.
     *
     * @param   header type.
     */
    public delete(header: string): void {
        delete this.header[header];
    }
    /**
     * get all header.
     */
    public entries(): { [key: string]: Head } {
        return this.header;
    }
    /**
     * hasEntries.
     *
     * @return  true / false
     */
    public hasEntries(): boolean {
        return Object.keys(this.header).length > 0;
    }
}

class Head {
    header: string;
    value: string;
}