import {RequestMethod} from "./Enum";
import {RequestOptionsArgs} from "./RequestOptionsArgs";

export class HttpRequestManager {

    static sendAsync(url: string, method: RequestMethod, options?: RequestOptionsArgs): Promise<any> {
        let xhr = new XMLHttpRequest();
		if(!options.params){
			options.params = '';
		}
        xhr.open(method.toString(), url + options.params);
        xhr.timeout = 5000;

        let elementCsrfToken = document.querySelector('meta[name="_csrf"]');
        if (elementCsrfToken && elementCsrfToken.getAttribute("content") !== "") {
            xhr.setRequestHeader(
                document.querySelector('meta[name="_csrf_header"]').getAttribute("content"),
                elementCsrfToken.getAttribute("content"));
        }

        if (options.headers.hasEntries()) {
            let entries = options.headers.entries();
            for (var key in entries) {
                xhr.setRequestHeader(entries[key].header, entries[key].value)
            }
        }

        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.UNSENT) {
                //console.log("[SalaryCheckJobletter.signon()] readyState 0: UNSENT.");
            }
            if (xhr.readyState === XMLHttpRequest.OPENED) {
                //console.log("[SalaryCheckJobletter.signon()] readyState 1: OPENED.");
            }
            if (xhr.readyState === XMLHttpRequest.HEADERS_RECEIVED) {
                //console.log("[SalaryCheckJobletter.signon()] readyState 2: HEADERS_RECEIVED.");
            }
            if (xhr.readyState === XMLHttpRequest.LOADING) {
                //console.log("[SalaryCheckJobletter.signon()] readyState 3: LOADING.");
            }
            if (xhr.readyState === XMLHttpRequest.DONE) {
                //console.log("[SalaryCheckJobletter.signon()] readyState 3: DONE.");
                //console.log("[SalaryCheckJobletter.signon()] status: %s", xhr.status);
            }
        };

        xhr.ontimeout = function () {
            console.log("Ups, da ist etwas schief gelaufen...");
        }

        xhr.send(options.body || null);

        return new Promise(function (resolve, reject) {
            xhr.onload = function () {
                if (this.status >= 200 && this.status <= 226) {
                    resolve(this);
                } else {
                    reject(this);
                }
            };

            xhr.ontimeout = function () {
                reject(this);
            };
        });
    }

    /**
     * @Deprecated
     * Sends a synchronous HTTP request. This functionality is deprecated in modern browsers and will be removed in the near future.
     * This method is intended for edge cases in which a page unload would otherwise cause an asynchronous request to be prematurely  aborted.
     * Use navigator.sendBeacon instead - see @Http.ts for an example
     * @param url
     * @param method
     */
    static sendSync(url: string, method: RequestMethod, options?: RequestOptionsArgs) {
        let request = new XMLHttpRequest();
        request.open(method.toString(), url, false)
        request.setRequestHeader(document.querySelector('meta[name="_csrf_header"]').getAttribute("content"), document.querySelector('meta[name="_csrf"]').getAttribute("content"));
        request.setRequestHeader("Content-Type", "application/json");
        if (options.headers != null && options.headers.hasEntries()) {
            let entries = options.headers.entries();
            for (var key in entries) {
                request.setRequestHeader(entries[key].header, entries[key].value)
            }
        }

        request.send(options.body || null);
    }
}
