import { Http } from "./http/Http.js";
import { Header } from "./http/Header.js";

export class SuggestFunction {

	inputfield: HTMLInputElement;
	callURL: string;
	currentFocus: number;
	callback: Function;
	loweringCase: boolean;

	constructor(inputfield: HTMLInputElement, callURL: string, loweringCase: boolean, callback: Function) {

		this.inputfield = inputfield;
		this.callURL = callURL;
		this.callback = callback;
		this.loweringCase = loweringCase;
		this.bindBehaviour();
	}

	bindBehaviour() {
		let self = this;

		document.addEventListener("click", function(e) {
			self.closeAllLists(e.target);
		});

		self.inputfield.addEventListener("input", function(e) {
			let a: HTMLElement, b: HTMLElement, val: string = this.value;
			/*close any already open lists of autocompleted values*/
			self.closeAllLists(null);
			if (!val) { return false; }
			self.currentFocus = -1;
			/*create a DIV element that will contain the items (values):*/
			a = document.createElement("DIV");
			a.setAttribute("id", this.id + "autocomplete-list");
			a.setAttribute("class", "autocomplete-items");
			/*append the DIV element as a child of the autocomplete container:*/
			this.parentNode.appendChild(a);
			/*for each item in the array...*/
			let searchTextFromField: string;
			if (self.loweringCase) {
				searchTextFromField = self.inputfield.value.toLowerCase();
			} else {
				searchTextFromField = self.inputfield.value;
			}
			let http = new Http();
			http.get(self.callURL, {
				headers: new Header(),
				params: ('?searchText=' + searchTextFromField)
			}).then((res) => {
				let data: string[] = JSON.parse((res as XMLHttpRequest).responseText);
				for (let i = 0; i < data.length; i++) {
					let startOfSearchtext: number = data[i].indexOf(searchTextFromField)
					// index can be -1 because searchtext with space can generate fals posetiv result from h2 database
					if (startOfSearchtext >= 0) {
						/*check if the item starts with the same letters as the text field value:*/
						/*create a DIV element for each matching element:*/
						b = document.createElement("DIV");
						/*make the matching letters bold:*/

						b.innerHTML = '';
						if (startOfSearchtext > 0) {
							b.innerHTML += data[i].substr(0, startOfSearchtext);
						}
						b.innerHTML += "<strong>" + data[i].substr(startOfSearchtext, searchTextFromField.length) + "</strong>";
						b.innerHTML += data[i].substr(startOfSearchtext + searchTextFromField.length, data[i].length);
						/*insert a input field that will hold the current array item's value:*/
						b.innerHTML += "<input type='hidden' value='" + data[i] + "'>";
						/*execute a function when someone clicks on the item value (DIV element):*/
						b.addEventListener("click", function(e) {
							/*insert the value for the autocomplete text field:*/
							self.inputfield.value = '';
							self.callback(this.getElementsByTagName("input")[0].value);
							/*close the list of autocompleted values,
							(or any other open lists of autocompleted values:*/
							self.closeAllLists(null);
						});
						a.appendChild(b);
					}
				}
			});
		});
		/*execute a function presses a key on the keyboard:*/
		self.inputfield.addEventListener("keydown", function(e) {
			let x: any = document.getElementById(this.id + "autocomplete-list");
			if (x) {
				x = x.getElementsByTagName("div");
			}
			if (e.keyCode == 40) {
				/*If the arrow DOWN key is pressed,
				increase the currentFocus variable:*/
				self.currentFocus++;
				/*and and make the current item more visible:*/
				self.addActive(x);
			} else if (e.keyCode == 38) { //up
				/*If the arrow UP key is pressed,
				decrease the currentFocus variable:*/
				self.currentFocus--;
				/*and and make the current item more visible:*/
				self.addActive(x);
			} else if (e.keyCode == 13) {
				/*If the ENTER key is pressed, prevent the form from being submitted,*/
				e.preventDefault();
				if (self.currentFocus > -1) {
					/*and simulate a click on the "active" item:*/
					if (x) x[self.currentFocus].click();
				}
			}
		});
	}


	addActive(x: HTMLElement[]) {
		/*a function to classify an item as "active":*/
		if (!x) return false;
		/*start by removing the "active" class on all items:*/
		this.removeActive(x);
		if (this.currentFocus >= x.length) {
			this.currentFocus = 0;
		}
		if (this.currentFocus < 0) {
			this.currentFocus = (x.length - 1);
		}
		/*add class "autocomplete-active":*/
		x[this.currentFocus].classList.add("autocomplete-active");
	}

	removeActive(x: HTMLElement[]) {
		/*a function to remove the "active" class from all autocomplete items:*/
		for (let i = 0; i < x.length; i++) {
			x[i].classList.remove("autocomplete-active");
		}
	}

	closeAllLists(elmnt: EventTarget) {
		/*close all autocomplete lists in the document,
		except the one passed as an argument:*/
		let x = document.getElementsByClassName("autocomplete-items");
		for (let i = 0; i < x.length; i++) {
			if (elmnt != x[i] && elmnt != this.inputfield) {
				x[i].parentNode.removeChild(x[i]);
			}
		}
	}
}
