
export class Pagination {

	buttonContainer: HTMLElement;
	backButton: HTMLButtonElement;
	ahaedButton: HTMLButtonElement;
	pageNr: number;
	pageSize: number;
	elementsToShow: number;
	callback: Function;

	constructor(buttonContainer: HTMLElement, minPageSize: number, callback: Function) {
		this.pageNr = 0;
		this.callback = callback;
		this.elementsToShow = minPageSize;
		this.pageSize = minPageSize + 1;
		this.buttonContainer = buttonContainer;
		this.setUpPaginationButtons();
		this.bindBehavior();
	}

	setUpPaginationButtons() {
		this.backButton = document.createElement('button');
		this.backButton.id = 'backbutton';
		this.backButton.classList.add('col-lg-1', 'btn', 'btn-info', 'glyphicon', 'glyphicon-chevron-left', 'disabled');
		this.buttonContainer.appendChild(this.backButton);

		let spaceHolder = document.createElement('div');
		spaceHolder.classList.add('col-lg-10');
		this.buttonContainer.appendChild(spaceHolder);

		this.ahaedButton = document.createElement('button');
		this.ahaedButton.id = 'ahaedButton';
		this.ahaedButton.classList.add('col-lg-1', 'btn', 'btn-info', 'glyphicon', 'glyphicon-chevron-right', 'disabled');
		this.buttonContainer.appendChild(this.ahaedButton);
	}

	bindBehavior() {
		let self = this;

		this.backButton.addEventListener('click', function(e) {
			if (self.backButton.classList.contains('active')) {
				self.pageNr--;
				self.callback(self.pageNr, self.pageSize, self.updateNavigationButtons.bind(self));
			}
		});

		this.ahaedButton.addEventListener('click', function(e) {
			if (self.ahaedButton.classList.contains('active')) {
				self.pageNr++;
				self.callback(self.pageNr, self.pageSize, self.updateNavigationButtons.bind(self));
			}
		});
	}

	updateNavigationButtons(resultCount: number) {
		this.hasPrevPage();
		this.hasNextPage(resultCount);
	}

	hasPrevPage() {
		if (this.pageNr < 1) {
			this.deactivateButton(this.backButton);
		} else {
			this.activateButton(this.backButton);
		}
	}

	hasNextPage(resultCount: number) {
		if (resultCount > this.elementsToShow) {
			this.activateButton(this.ahaedButton);
		} else {
			this.deactivateButton(this.ahaedButton);
		}
	}

	activateButton(button: HTMLButtonElement) {
		button.classList.remove('disabled');
		button.classList.add('active');
	}

	deactivateButton(button: HTMLButtonElement) {
		button.classList.remove('active');
		button.classList.add('disabled');
	}
}