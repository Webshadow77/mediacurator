class RegisterPage {

	passWd1: HTMLInputElement;
	passWd2: HTMLInputElement;

	constructor() {
		this.passWd1 = document.getElementById('password1') as HTMLInputElement;
		this.passWd2 = document.getElementById('password2') as HTMLInputElement;

		this.checkValPasswd();
		this.bindBehaviour();

	}

	bindBehaviour() {
		let self = this;
		this.passWd1.addEventListener("keyup", function() {
			self.checkValPasswd();
		});
		this.passWd2.addEventListener("keyup", function() {
			self.checkValPasswd();
		});
	}

	checkValPasswd() {
		if (!(this.passWd1.value === '' || this.passWd1.value === undefined
			|| this.passWd2.value === '' || this.passWd2.value === undefined)
			&& this.passWd1.value === this.passWd2.value) {
			document.getElementById('registerBtn').style.display = 'block';
		} else {
			document.getElementById('registerBtn').style.display = 'none';
		}
	}
}

let registerPage:RegisterPage = new RegisterPage();